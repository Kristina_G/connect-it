﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ConnectIT.Data.Migrations
{
    public partial class addUserProps : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOn",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedOn",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOn",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "781a51b3-f134-4112-87f3-13b31f67ff27",
                column: "ConcurrencyStamp",
                value: "4d266b83-21ee-4447-b53c-5bf7be4b8959");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2388891b-5a68-47bd-9b21-055c319ca23c",
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash", "SecurityStamp" },
                values: new object[] { "882a32ee-fe5e-4ab1-a77a-9d17950f1ef9", new DateTime(2020, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "AQAAAAEAACcQAAAAECHvs5e2w/e4YYlRzTrOwdaJcsrXz2h2JeLtn5bB595OoaBtTjsOK90oxz9RCs0SCA==", "54ff4287-ca7d-4a75-b967-213e69a039ce" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "24cb77bf-a074-46ba-b3e5-27e81e52d73a",
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash", "SecurityStamp" },
                values: new object[] { "3ac77754-e80c-4634-9940-bbe6fe2b28f0", new DateTime(2020, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "AQAAAAEAACcQAAAAEHL75xwHvuCCjlqso1De17AC9f49R+uFaEXJdx2dmhgpD740Xeru0kusmXmLf6ZDtw==", "acd4bec0-e408-4051-8e6b-17c1ebc346cd" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4f3c09b2-f4c6-46c0-9b0b-216cb08f6184",
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b7334896-466d-4a49-9b82-2c4886deed3e", new DateTime(2020, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "AQAAAAEAACcQAAAAEJYJBVXP7xsDM72C4o24Ee88Ev+USntdG4dtkz8Su8ViJN5SSQVGNX0JU0munGQI5Q==", "8f1199f9-c323-4981-a27d-1698699e0c92" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "988d99d9-ccb5-4f72-b6b4-c318cb1c6805",
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9d4db6d4-0db3-43b0-887b-a7c9afdf358a", new DateTime(2020, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "AQAAAAEAACcQAAAAEGUd5h8IFM3HKy5Uric1dW34NgtH7cQvdIBwlG2We6CXtAkN26cypQ/b7x2oact8yw==", "a8f5d2ad-d16e-4e17-a9d6-9ea17154a9ad" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8",
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash", "SecurityStamp" },
                values: new object[] { "4d7fcaff-9d63-470b-9e87-6b1a0dc9de82", new DateTime(2020, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "AQAAAAEAACcQAAAAEMhuMwj8kgD0KvOfYA1l/4UWQevyghwueNiDRQ6zCtb51HYie5tVopDkGCXHOHz9LA==", "389afda5-fe06-478b-9c16-7e6132583776" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd1ac498-a477-40bf-8526-7c2208adbad5",
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash", "SecurityStamp" },
                values: new object[] { "4c36419b-3754-4536-8ead-45b870d946c5", new DateTime(2020, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "AQAAAAEAACcQAAAAEPiklLVe9YhIK6ZD1if+Z1CHbLNKWA7YAwLWnlUBIbMxOojQoK3VRgOTiimGCJ/mtg==", "0c581e19-7949-44c7-85c2-67fc5f358e16" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedOn",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "DeletedOn",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ModifiedOn",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "781a51b3-f134-4112-87f3-13b31f67ff27",
                column: "ConcurrencyStamp",
                value: "3d54aaf6-3d67-4e69-84c5-c24eccf89d16");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2388891b-5a68-47bd-9b21-055c319ca23c",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "e0c3cab3-91ba-4ab3-9a1b-738204ed6582", "AQAAAAEAACcQAAAAEAgg7cte/exwBx/s111sDjr3I/EmKsXASXeoe6sB9S7bDfw2tmLYU38NpCZdRd2ctw==", "ebfe41f1-6f9d-40ca-9676-f73a656f939a" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "24cb77bf-a074-46ba-b3e5-27e81e52d73a",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "cf9e5b08-1e15-4b7f-a02c-73144bc780c8", "AQAAAAEAACcQAAAAEGdgbi+iSdskR2AudBu+8WPk8QFgBgU2VBe167EW/bbc5oAdWAYIOVspG1pLfAkWTw==", "2b534055-a1ef-49a2-b809-73733734c039" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4f3c09b2-f4c6-46c0-9b0b-216cb08f6184",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "a54ae285-a4e8-4c34-9649-1837e32f59aa", "AQAAAAEAACcQAAAAEFKHf9zc3x+q7SCQEu36Ko5jLuxnjS4C5NczH8EfQ5mJ9SAckrkUcf8P0+SCagwzIA==", "2c7b064a-bc0d-4eb5-8e69-c95cd0b03bf3" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "988d99d9-ccb5-4f72-b6b4-c318cb1c6805",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "d9cdf3f2-0295-434b-a395-49ff09db5f00", "AQAAAAEAACcQAAAAEDkQXde8DTpyNpfA+P/RpTeXpaoxx6vYJPUd2/xJEbeoWaBTiPNCDjM8b6M/U/qYaA==", "90b6c644-9590-4ba2-86d3-194c2bb89b93" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0e005468-778d-4036-9d56-8b2a917c8a53", "AQAAAAEAACcQAAAAEIWS2TvpNlzsdzITCTIuplY4ycc2RUeY22VIKpOJu0SsidPZO0fOpcazGq51ZFG3yw==", "61d4cb50-2e9a-4241-9757-9183ee03c249" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd1ac498-a477-40bf-8526-7c2208adbad5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "ea73cf3b-be5a-4edd-98e4-5fb5eff9827d", "AQAAAAEAACcQAAAAEBMiZl4mcHuZFGVVrhCIQrBl4i3jROJY4sg0OBT+FdtBjLP3yfQgQdbiLw7/lsTAew==", "f0906628-b5fb-4d65-b058-b70a1282cb0f" });
        }
    }
}
