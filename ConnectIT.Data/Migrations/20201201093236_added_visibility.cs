﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ConnectIT.Data.Migrations
{
    public partial class added_visibility : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "PubliclyVisible",
                table: "Posts",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "HideProfileDetails",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "781a51b3-f134-4112-87f3-13b31f67ff27",
                column: "ConcurrencyStamp",
                value: "071ed90e-8a91-499f-8585-72cdb27d59c3");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2388891b-5a68-47bd-9b21-055c319ca23c",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "480723b8-2286-4e28-87d0-e987c29e20fb", "AQAAAAEAACcQAAAAEA6FQmvYywtuT1ZWIaW110wlnnw09xSCL8doDFZ6yp/WRjEt/WGQmxF3uG68k+45Wg==", "c30f3377-2406-4e88-95a3-b90ec731ba2e" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "24cb77bf-a074-46ba-b3e5-27e81e52d73a",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "fd884de1-bca2-41ac-8e58-91e13538fb82", "AQAAAAEAACcQAAAAENkHn6RlN690ZpaMDIApgzRaKxn5McA0NS9D5nuZNv3XdpqgEarxx+0KK4oUKJDe5A==", "c0b55d7f-7ff4-4325-af4c-b69862c3f5b0" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4f3c09b2-f4c6-46c0-9b0b-216cb08f6184",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0a18dfa0-1991-4291-aec8-19488e1d6f03", "AQAAAAEAACcQAAAAEC0pQrtgarVeE/t3fQ6ihFdpXs+JsdnOEfRUZR3U3XrgAopg5bvGxG+VyzRtGJ9G1Q==", "2759c595-4962-4b98-ae8e-d6a1b200b984" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "988d99d9-ccb5-4f72-b6b4-c318cb1c6805",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "4df24609-a5f8-4bb8-84e1-18f26addcec1", "AQAAAAEAACcQAAAAEG3A9QpwAz+Kh9GjvQ5tQo4q6p5mb77Orxu/8z3h9Xebe2TGuy/+7MUI6srjcIyqfQ==", "eeff024a-58ef-4a5b-a727-2772de72b910" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "d2f776e9-5145-4a6c-ab50-c1f724fc995d", "AQAAAAEAACcQAAAAEEFniQCLwOx27GHh1VIckKZ0aOJMoovWeYz6Lwpiw8QpZtseGyBzQSyVQ8Kp0uUFeQ==", "69008051-6d00-4452-980e-08289373bd3f" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd1ac498-a477-40bf-8526-7c2208adbad5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "693f87da-9710-417a-bf02-3ef382dad65e", "AQAAAAEAACcQAAAAEPt50HoqrwPoKdAa/utLTq/o4BXT15aPfdCRso6yrTItwf8gb1tH0MG42BLH/e8PpQ==", "8b3dce8c-5141-49c9-9973-1dcd81cc77f3" });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                column: "PubliclyVisible",
                value: true);

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                column: "PubliclyVisible",
                value: true);

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                column: "PubliclyVisible",
                value: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PubliclyVisible",
                table: "Posts");

            migrationBuilder.DropColumn(
                name: "HideProfileDetails",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "781a51b3-f134-4112-87f3-13b31f67ff27",
                column: "ConcurrencyStamp",
                value: "e24ce3d6-e430-44ec-bb92-a545a37abd8c");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2388891b-5a68-47bd-9b21-055c319ca23c",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "cda09231-701d-4320-9017-20a08742afbe", "AQAAAAEAACcQAAAAEGcGyufGsG4sd8VDEt6LghTqMk2QN6oTnWHN7s34fG4zjvqhirA6pqrHfYJOhgMulg==", "0584bcb1-dd9e-4ce0-b279-64f6a80a2838" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "24cb77bf-a074-46ba-b3e5-27e81e52d73a",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "bd53ffd3-8596-426a-b8e7-93620a961fb1", "AQAAAAEAACcQAAAAEBJqG+Z+TvP0rYnNXQZ27ThERYJt51/rJQSV0kNya2FLIPgYhXwnKzGvOKsArJUqVg==", "9362d314-81a0-4144-b2f5-69db95fc4f24" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4f3c09b2-f4c6-46c0-9b0b-216cb08f6184",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "398ea516-3307-43c1-a588-9137b48befa5", "AQAAAAEAACcQAAAAEAnzMC8mm5e3X70r0Np1i9KrjFWJjirCnmlrEzbGXkt8fcbbbDwIiaui7oyRWPT0Vg==", "4bec8255-7deb-4da2-9177-78cb929eb057" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "988d99d9-ccb5-4f72-b6b4-c318cb1c6805",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9176693c-f5c0-49a1-a4c2-1f3f5ddb1220", "AQAAAAEAACcQAAAAEMh7sSVXpFUfKvvbiaReKAQuDYXVGLFvNhxE6W8lmyj7JU/HSziGbBugMyxPhsLEYQ==", "01f910d2-c74b-4cf7-b3dc-7a10c747ed8d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "3c583c2f-f4c4-4c4c-b0ea-568eaee5b7d4", "AQAAAAEAACcQAAAAEDNLpWtmsRDjNEve+iScPMK2c/k98bZzAYmiZEeDy7UB88DKnjYc0mSEZ+JFKFIYdA==", "c940f12e-ff64-4575-ac5f-0e2cc945a30d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd1ac498-a477-40bf-8526-7c2208adbad5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "570bb72e-ad04-4653-8576-6eaf7676d6ef", "AQAAAAEAACcQAAAAENkOz2AhG+fG26gWvf8vErDayepT4ERwk2Adnyod51kXj0euWu2tK33XrQ6RXLjBjA==", "f2d7c320-a2d9-4439-9b58-7a9f2d0a6d12" });
        }
    }
}
