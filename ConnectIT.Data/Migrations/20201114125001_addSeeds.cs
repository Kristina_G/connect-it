﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ConnectIT.Data.Migrations
{
    public partial class addSeeds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "781a51b3-f134-4112-87f3-13b31f67ff27",
                column: "ConcurrencyStamp",
                value: "3d54aaf6-3d67-4e69-84c5-c24eccf89d16");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2388891b-5a68-47bd-9b21-055c319ca23c",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "e0c3cab3-91ba-4ab3-9a1b-738204ed6582", "AQAAAAEAACcQAAAAEAgg7cte/exwBx/s111sDjr3I/EmKsXASXeoe6sB9S7bDfw2tmLYU38NpCZdRd2ctw==", "ebfe41f1-6f9d-40ca-9676-f73a656f939a" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "24cb77bf-a074-46ba-b3e5-27e81e52d73a",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "cf9e5b08-1e15-4b7f-a02c-73144bc780c8", "AQAAAAEAACcQAAAAEGdgbi+iSdskR2AudBu+8WPk8QFgBgU2VBe167EW/bbc5oAdWAYIOVspG1pLfAkWTw==", "2b534055-a1ef-49a2-b809-73733734c039" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4f3c09b2-f4c6-46c0-9b0b-216cb08f6184",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "a54ae285-a4e8-4c34-9649-1837e32f59aa", "AQAAAAEAACcQAAAAEFKHf9zc3x+q7SCQEu36Ko5jLuxnjS4C5NczH8EfQ5mJ9SAckrkUcf8P0+SCagwzIA==", "2c7b064a-bc0d-4eb5-8e69-c95cd0b03bf3" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "988d99d9-ccb5-4f72-b6b4-c318cb1c6805",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "d9cdf3f2-0295-434b-a395-49ff09db5f00", "AQAAAAEAACcQAAAAEDkQXde8DTpyNpfA+P/RpTeXpaoxx6vYJPUd2/xJEbeoWaBTiPNCDjM8b6M/U/qYaA==", "90b6c644-9590-4ba2-86d3-194c2bb89b93" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0e005468-778d-4036-9d56-8b2a917c8a53", "AQAAAAEAACcQAAAAEIWS2TvpNlzsdzITCTIuplY4ycc2RUeY22VIKpOJu0SsidPZO0fOpcazGq51ZFG3yw==", "61d4cb50-2e9a-4241-9757-9183ee03c249" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd1ac498-a477-40bf-8526-7c2208adbad5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "ea73cf3b-be5a-4edd-98e4-5fb5eff9827d", "AQAAAAEAACcQAAAAEBMiZl4mcHuZFGVVrhCIQrBl4i3jROJY4sg0OBT+FdtBjLP3yfQgQdbiLw7/lsTAew==", "f0906628-b5fb-4d65-b058-b70a1282cb0f" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "781a51b3-f134-4112-87f3-13b31f67ff27",
                column: "ConcurrencyStamp",
                value: "c2ceaab5-06b0-41ab-b863-c1e6a558cd20");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2388891b-5a68-47bd-9b21-055c319ca23c",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "7a20569e-01c7-4098-85e0-8a58406c0187", "AQAAAAEAACcQAAAAEGgXF8E8Cp+g2HjSfE60xgq1BiZH2s4x3K8khmvo8/zO7bBSEGuqVGOCmT02yUaWmA==", "90d8d279-1037-4e70-baad-56396dd60420" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "24cb77bf-a074-46ba-b3e5-27e81e52d73a",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "bd748f13-2cea-4a66-b419-07567f995ca1", "AQAAAAEAACcQAAAAEPFZztNFpeQG7SdGvj6DkINYf2EkjSbmXxo4e0hnEHeSFmgE79syQueD6G2l+++PQg==", "6e3ca322-918d-4746-bc92-93742ec3c9c3" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4f3c09b2-f4c6-46c0-9b0b-216cb08f6184",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "e553ad3e-e1d1-4019-8d7d-f17e7fcd8a4a", "AQAAAAEAACcQAAAAELCFx3gdsYBiPOZRRE0tU9qxzGYVUXBz51BcDa57rk/jzuo9iKdBy+4omGz2ZNBujA==", "ea63a531-7b3f-4a2b-ab72-5db1117a89c6" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "988d99d9-ccb5-4f72-b6b4-c318cb1c6805",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0095b2cf-0a93-497a-80c8-3f7d079a7a9e", "AQAAAAEAACcQAAAAEG1yjc3w+zmSK7hvTQsyfFbXwDGynFuCQhV9fGNlQ1MOMpAhIG4saBNqs8eueW3MAg==", "43ed5982-379e-4dea-b596-9a89f098cbd6" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "48879200-24e4-4436-84ea-a0461f61173c", "AQAAAAEAACcQAAAAEHBKjvYJC8B6aH8DGez7jToe8TF0wQplOWIvtIwcyHL6GEzUb7ZAyWblhNL9a6CndQ==", "737dd62a-1386-4968-a90e-83a17b5faec8" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd1ac498-a477-40bf-8526-7c2208adbad5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "c5b9d5c2-b901-437c-96a0-06999351992f", "AQAAAAEAACcQAAAAEG/pNJf+RdSnC9kbM4PjwFvUQANjXUX/h4Uxu9UTHQxpPDboraBIJKHZjFbYnDQYbg==", "1fa12b11-8610-4908-b8e8-a773b8008344" });
        }
    }
}
