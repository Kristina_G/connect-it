﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ConnectIT.Data.Migrations
{
    public partial class Friendship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FriendRequest_AspNetUsers_ReceiverId",
                table: "FriendRequest");

            migrationBuilder.DropForeignKey(
                name: "FK_FriendRequest_AspNetUsers_SenderId",
                table: "FriendRequest");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FriendRequest",
                table: "FriendRequest");

            migrationBuilder.RenameTable(
                name: "FriendRequest",
                newName: "FriendRequests");

            migrationBuilder.RenameIndex(
                name: "IX_FriendRequest_ReceiverId",
                table: "FriendRequests",
                newName: "IX_FriendRequests_ReceiverId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_FriendRequests",
                table: "FriendRequests",
                columns: new[] { "SenderId", "ReceiverId" });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "781a51b3-f134-4112-87f3-13b31f67ff27",
                column: "ConcurrencyStamp",
                value: "e24ce3d6-e430-44ec-bb92-a545a37abd8c");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2388891b-5a68-47bd-9b21-055c319ca23c",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "cda09231-701d-4320-9017-20a08742afbe", "AQAAAAEAACcQAAAAEGcGyufGsG4sd8VDEt6LghTqMk2QN6oTnWHN7s34fG4zjvqhirA6pqrHfYJOhgMulg==", "0584bcb1-dd9e-4ce0-b279-64f6a80a2838" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "24cb77bf-a074-46ba-b3e5-27e81e52d73a",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "bd53ffd3-8596-426a-b8e7-93620a961fb1", "AQAAAAEAACcQAAAAEBJqG+Z+TvP0rYnNXQZ27ThERYJt51/rJQSV0kNya2FLIPgYhXwnKzGvOKsArJUqVg==", "9362d314-81a0-4144-b2f5-69db95fc4f24" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4f3c09b2-f4c6-46c0-9b0b-216cb08f6184",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "398ea516-3307-43c1-a588-9137b48befa5", "AQAAAAEAACcQAAAAEAnzMC8mm5e3X70r0Np1i9KrjFWJjirCnmlrEzbGXkt8fcbbbDwIiaui7oyRWPT0Vg==", "4bec8255-7deb-4da2-9177-78cb929eb057" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "988d99d9-ccb5-4f72-b6b4-c318cb1c6805",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9176693c-f5c0-49a1-a4c2-1f3f5ddb1220", "AQAAAAEAACcQAAAAEMh7sSVXpFUfKvvbiaReKAQuDYXVGLFvNhxE6W8lmyj7JU/HSziGbBugMyxPhsLEYQ==", "01f910d2-c74b-4cf7-b3dc-7a10c747ed8d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "3c583c2f-f4c4-4c4c-b0ea-568eaee5b7d4", "AQAAAAEAACcQAAAAEDNLpWtmsRDjNEve+iScPMK2c/k98bZzAYmiZEeDy7UB88DKnjYc0mSEZ+JFKFIYdA==", "c940f12e-ff64-4575-ac5f-0e2cc945a30d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd1ac498-a477-40bf-8526-7c2208adbad5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "570bb72e-ad04-4653-8576-6eaf7676d6ef", "AQAAAAEAACcQAAAAENkOz2AhG+fG26gWvf8vErDayepT4ERwk2Adnyod51kXj0euWu2tK33XrQ6RXLjBjA==", "f2d7c320-a2d9-4439-9b58-7a9f2d0a6d12" });

            migrationBuilder.AddForeignKey(
                name: "FK_FriendRequests_AspNetUsers_ReceiverId",
                table: "FriendRequests",
                column: "ReceiverId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FriendRequests_AspNetUsers_SenderId",
                table: "FriendRequests",
                column: "SenderId",
                principalTable: "AspNetUsers",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FriendRequests_AspNetUsers_ReceiverId",
                table: "FriendRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_FriendRequests_AspNetUsers_SenderId",
                table: "FriendRequests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FriendRequests",
                table: "FriendRequests");

            migrationBuilder.RenameTable(
                name: "FriendRequests",
                newName: "FriendRequest");

            migrationBuilder.RenameIndex(
                name: "IX_FriendRequests_ReceiverId",
                table: "FriendRequest",
                newName: "IX_FriendRequest_ReceiverId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_FriendRequest",
                table: "FriendRequest",
                columns: new[] { "SenderId", "ReceiverId" });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "781a51b3-f134-4112-87f3-13b31f67ff27",
                column: "ConcurrencyStamp",
                value: "9c12d6a6-a117-4820-a7c8-f27dcdcdd44a");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2388891b-5a68-47bd-9b21-055c319ca23c",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "6227ac7e-a49d-486d-ab37-c8dc3d8120f3", "AQAAAAEAACcQAAAAELcRjLa+xRTF0+icy2dkWxdY2NUGEISyXbLASO4lP4+01X82FqmcpPtUhW4R1Sn2KA==", "5d8a795e-c33a-44a8-91ea-c7d31ab47a20" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "24cb77bf-a074-46ba-b3e5-27e81e52d73a",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "1508c03f-9af4-4cd0-ba8f-d4cd11c7dd68", "AQAAAAEAACcQAAAAEJtaZwT2+1MX7xCpF0Ww1F8DxczVf93hOZwfd9+sNac82s1/nNsHJJtOfL+0zYwzDw==", "802827b8-92fe-4127-acd7-1902b0401096" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4f3c09b2-f4c6-46c0-9b0b-216cb08f6184",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "a642e8c8-6f61-4036-8026-fc986d0a2f1d", "AQAAAAEAACcQAAAAEF1ihGyPJzBwPVJBaAryX/ph3zBL6ayka00wjrlaz73JLf2rpzYGtRvB5c6Ao18cXQ==", "e3903b69-acdb-4d43-ae15-3f946cf4fb43" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "988d99d9-ccb5-4f72-b6b4-c318cb1c6805",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "4734b817-c487-4ccd-8ef3-daa32a29e42a", "AQAAAAEAACcQAAAAEOGTolyxbtty0e/8bWszxaQA3h7dXDQFrA4osZYpsFCfLaCuJzbz7eXUQfbiL/jT/g==", "23e53136-e02e-449a-a8c7-0771897e05c2" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "496c1d2b-a71a-43be-9025-65519ce94edf", "AQAAAAEAACcQAAAAEFOsprV4ikKu/gk7qTwTAJZyOX0E6FcX+TP6iXNaM1ewwkPPcUnTbrvxGkLa/xEMxA==", "0087b62a-dcd8-41e0-be5c-5796049fbec0" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd1ac498-a477-40bf-8526-7c2208adbad5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "91c08d3b-baab-4e60-ac60-ae5d42f7afba", "AQAAAAEAACcQAAAAEBhedsLmCqmeWgLpc3KVd+OalTmGjYfRrtx5hA6O6ZOPpiwNLHn4QeqFnft73BZuDQ==", "02e20287-53bf-4e21-ac91-31b758377fce" });

            migrationBuilder.AddForeignKey(
                name: "FK_FriendRequest_AspNetUsers_ReceiverId",
                table: "FriendRequest",
                column: "ReceiverId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FriendRequest_AspNetUsers_SenderId",
                table: "FriendRequest",
                column: "SenderId",
                principalTable: "AspNetUsers",
                principalColumn: "Id");
        }
    }
}
