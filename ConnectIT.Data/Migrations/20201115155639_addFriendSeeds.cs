﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ConnectIT.Data.Migrations
{
    public partial class addFriendSeeds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Posts_PostId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_PostId",
                table: "AspNetUsers");

            migrationBuilder.DeleteData(
                table: "FriendRequest",
                keyColumns: new[] { "SenderId", "ReceiverId" },
                keyValues: new object[] { "24cb77bf-a074-46ba-b3e5-27e81e52d73a", "fd1ac498-a477-40bf-8526-7c2208adbad5" });

            migrationBuilder.DeleteData(
                table: "FriendRequest",
                keyColumns: new[] { "SenderId", "ReceiverId" },
                keyValues: new object[] { "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8", "2388891b-5a68-47bd-9b21-055c319ca23c" });

            migrationBuilder.DeleteData(
                table: "FriendRequest",
                keyColumns: new[] { "SenderId", "ReceiverId" },
                keyValues: new object[] { "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8", "24cb77bf-a074-46ba-b3e5-27e81e52d73a" });

            migrationBuilder.DropColumn(
                name: "PostId",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<int>(
                name: "PostId",
                table: "Friend",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "781a51b3-f134-4112-87f3-13b31f67ff27",
                column: "ConcurrencyStamp",
                value: "9c12d6a6-a117-4820-a7c8-f27dcdcdd44a");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2388891b-5a68-47bd-9b21-055c319ca23c",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "6227ac7e-a49d-486d-ab37-c8dc3d8120f3", "AQAAAAEAACcQAAAAELcRjLa+xRTF0+icy2dkWxdY2NUGEISyXbLASO4lP4+01X82FqmcpPtUhW4R1Sn2KA==", "5d8a795e-c33a-44a8-91ea-c7d31ab47a20" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "24cb77bf-a074-46ba-b3e5-27e81e52d73a",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "1508c03f-9af4-4cd0-ba8f-d4cd11c7dd68", "AQAAAAEAACcQAAAAEJtaZwT2+1MX7xCpF0Ww1F8DxczVf93hOZwfd9+sNac82s1/nNsHJJtOfL+0zYwzDw==", "802827b8-92fe-4127-acd7-1902b0401096" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4f3c09b2-f4c6-46c0-9b0b-216cb08f6184",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "a642e8c8-6f61-4036-8026-fc986d0a2f1d", "AQAAAAEAACcQAAAAEF1ihGyPJzBwPVJBaAryX/ph3zBL6ayka00wjrlaz73JLf2rpzYGtRvB5c6Ao18cXQ==", "e3903b69-acdb-4d43-ae15-3f946cf4fb43" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "988d99d9-ccb5-4f72-b6b4-c318cb1c6805",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "4734b817-c487-4ccd-8ef3-daa32a29e42a", "AQAAAAEAACcQAAAAEOGTolyxbtty0e/8bWszxaQA3h7dXDQFrA4osZYpsFCfLaCuJzbz7eXUQfbiL/jT/g==", "23e53136-e02e-449a-a8c7-0771897e05c2" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "496c1d2b-a71a-43be-9025-65519ce94edf", "AQAAAAEAACcQAAAAEFOsprV4ikKu/gk7qTwTAJZyOX0E6FcX+TP6iXNaM1ewwkPPcUnTbrvxGkLa/xEMxA==", "0087b62a-dcd8-41e0-be5c-5796049fbec0" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd1ac498-a477-40bf-8526-7c2208adbad5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "91c08d3b-baab-4e60-ac60-ae5d42f7afba", "AQAAAAEAACcQAAAAEBhedsLmCqmeWgLpc3KVd+OalTmGjYfRrtx5hA6O6ZOPpiwNLHn4QeqFnft73BZuDQ==", "02e20287-53bf-4e21-ac91-31b758377fce" });

            migrationBuilder.InsertData(
                table: "Friend",
                columns: new[] { "SenderId", "ReceiverId", "PostId" },
                values: new object[,]
                {
                    { "24cb77bf-a074-46ba-b3e5-27e81e52d73a", "fd1ac498-a477-40bf-8526-7c2208adbad5", null },
                    { "24cb77bf-a074-46ba-b3e5-27e81e52d73a", "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8", null },
                    { "24cb77bf-a074-46ba-b3e5-27e81e52d73a", "2388891b-5a68-47bd-9b21-055c319ca23c", null },
                    { "fd1ac498-a477-40bf-8526-7c2208adbad5", "24cb77bf-a074-46ba-b3e5-27e81e52d73a", null },
                    { "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8", "24cb77bf-a074-46ba-b3e5-27e81e52d73a", null },
                    { "2388891b-5a68-47bd-9b21-055c319ca23c", "24cb77bf-a074-46ba-b3e5-27e81e52d73a", null }
                });

            migrationBuilder.InsertData(
                table: "FriendRequest",
                columns: new[] { "SenderId", "ReceiverId" },
                values: new object[,]
                {
                    { "fd1ac498-a477-40bf-8526-7c2208adbad5", "2388891b-5a68-47bd-9b21-055c319ca23c" },
                    { "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8", "fd1ac498-a477-40bf-8526-7c2208adbad5" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Friend_PostId",
                table: "Friend",
                column: "PostId");

            migrationBuilder.AddForeignKey(
                name: "FK_Friend_Posts_PostId",
                table: "Friend",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Friend_Posts_PostId",
                table: "Friend");

            migrationBuilder.DropIndex(
                name: "IX_Friend_PostId",
                table: "Friend");

            migrationBuilder.DeleteData(
                table: "Friend",
                keyColumns: new[] { "SenderId", "ReceiverId" },
                keyValues: new object[] { "2388891b-5a68-47bd-9b21-055c319ca23c", "24cb77bf-a074-46ba-b3e5-27e81e52d73a" });

            migrationBuilder.DeleteData(
                table: "Friend",
                keyColumns: new[] { "SenderId", "ReceiverId" },
                keyValues: new object[] { "24cb77bf-a074-46ba-b3e5-27e81e52d73a", "2388891b-5a68-47bd-9b21-055c319ca23c" });

            migrationBuilder.DeleteData(
                table: "Friend",
                keyColumns: new[] { "SenderId", "ReceiverId" },
                keyValues: new object[] { "24cb77bf-a074-46ba-b3e5-27e81e52d73a", "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8" });

            migrationBuilder.DeleteData(
                table: "Friend",
                keyColumns: new[] { "SenderId", "ReceiverId" },
                keyValues: new object[] { "24cb77bf-a074-46ba-b3e5-27e81e52d73a", "fd1ac498-a477-40bf-8526-7c2208adbad5" });

            migrationBuilder.DeleteData(
                table: "Friend",
                keyColumns: new[] { "SenderId", "ReceiverId" },
                keyValues: new object[] { "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8", "24cb77bf-a074-46ba-b3e5-27e81e52d73a" });

            migrationBuilder.DeleteData(
                table: "Friend",
                keyColumns: new[] { "SenderId", "ReceiverId" },
                keyValues: new object[] { "fd1ac498-a477-40bf-8526-7c2208adbad5", "24cb77bf-a074-46ba-b3e5-27e81e52d73a" });

            migrationBuilder.DeleteData(
                table: "FriendRequest",
                keyColumns: new[] { "SenderId", "ReceiverId" },
                keyValues: new object[] { "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8", "fd1ac498-a477-40bf-8526-7c2208adbad5" });

            migrationBuilder.DeleteData(
                table: "FriendRequest",
                keyColumns: new[] { "SenderId", "ReceiverId" },
                keyValues: new object[] { "fd1ac498-a477-40bf-8526-7c2208adbad5", "2388891b-5a68-47bd-9b21-055c319ca23c" });

            migrationBuilder.DropColumn(
                name: "PostId",
                table: "Friend");

            migrationBuilder.AddColumn<int>(
                name: "PostId",
                table: "AspNetUsers",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "781a51b3-f134-4112-87f3-13b31f67ff27",
                column: "ConcurrencyStamp",
                value: "4d266b83-21ee-4447-b53c-5bf7be4b8959");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2388891b-5a68-47bd-9b21-055c319ca23c",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "882a32ee-fe5e-4ab1-a77a-9d17950f1ef9", "AQAAAAEAACcQAAAAECHvs5e2w/e4YYlRzTrOwdaJcsrXz2h2JeLtn5bB595OoaBtTjsOK90oxz9RCs0SCA==", "54ff4287-ca7d-4a75-b967-213e69a039ce" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "24cb77bf-a074-46ba-b3e5-27e81e52d73a",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "3ac77754-e80c-4634-9940-bbe6fe2b28f0", "AQAAAAEAACcQAAAAEHL75xwHvuCCjlqso1De17AC9f49R+uFaEXJdx2dmhgpD740Xeru0kusmXmLf6ZDtw==", "acd4bec0-e408-4051-8e6b-17c1ebc346cd" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4f3c09b2-f4c6-46c0-9b0b-216cb08f6184",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b7334896-466d-4a49-9b82-2c4886deed3e", "AQAAAAEAACcQAAAAEJYJBVXP7xsDM72C4o24Ee88Ev+USntdG4dtkz8Su8ViJN5SSQVGNX0JU0munGQI5Q==", "8f1199f9-c323-4981-a27d-1698699e0c92" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "988d99d9-ccb5-4f72-b6b4-c318cb1c6805",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9d4db6d4-0db3-43b0-887b-a7c9afdf358a", "AQAAAAEAACcQAAAAEGUd5h8IFM3HKy5Uric1dW34NgtH7cQvdIBwlG2We6CXtAkN26cypQ/b7x2oact8yw==", "a8f5d2ad-d16e-4e17-a9d6-9ea17154a9ad" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "4d7fcaff-9d63-470b-9e87-6b1a0dc9de82", "AQAAAAEAACcQAAAAEMhuMwj8kgD0KvOfYA1l/4UWQevyghwueNiDRQ6zCtb51HYie5tVopDkGCXHOHz9LA==", "389afda5-fe06-478b-9c16-7e6132583776" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd1ac498-a477-40bf-8526-7c2208adbad5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "4c36419b-3754-4536-8ead-45b870d946c5", "AQAAAAEAACcQAAAAEPiklLVe9YhIK6ZD1if+Z1CHbLNKWA7YAwLWnlUBIbMxOojQoK3VRgOTiimGCJ/mtg==", "0c581e19-7949-44c7-85c2-67fc5f358e16" });

            migrationBuilder.InsertData(
                table: "FriendRequest",
                columns: new[] { "SenderId", "ReceiverId" },
                values: new object[,]
                {
                    { "24cb77bf-a074-46ba-b3e5-27e81e52d73a", "fd1ac498-a477-40bf-8526-7c2208adbad5" },
                    { "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8", "24cb77bf-a074-46ba-b3e5-27e81e52d73a" },
                    { "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8", "2388891b-5a68-47bd-9b21-055c319ca23c" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_PostId",
                table: "AspNetUsers",
                column: "PostId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Posts_PostId",
                table: "AspNetUsers",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
