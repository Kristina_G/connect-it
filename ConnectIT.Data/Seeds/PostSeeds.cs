﻿using ConnectIT.Data.Entities;
using System;

namespace ConnectIT.Data.Seeds
{
    internal static class PostSeeds
    {
        public static readonly Post[] posts = {
            new Post
            {
                Id = 1,
                AuthorId = UserSeeds.regular[0].Id,
                CreatedOn = new DateTime(2020, 11, 1),
                Content = "Laravel scores better than other web frameworks because of its advanced features" +
                " and development tools that facilitate rapid web application development.What are your thougts on this?" +
                "Has anyone used it recently?"
            },
            new Post
            {
                Id = 2,
                AuthorId = UserSeeds.regular[1].Id,
                CreatedOn = new DateTime(2020, 11, 1),
                Content = "With the release of Angular 6 it might be a good time to re-evaluate the use of the MVC pattern and therefore the value MVC Frameworks bring to Application Architecture." +
                 "Does anyone agree? "
            },

            new Post
            {
                Id = 3,
                AuthorId = UserSeeds.regular[3].Id,
                CreatedOn = new DateTime(2020, 11, 1),
                Content = "I recommend the Java Based wicked Framework, It's a component based MVC library which is really easy to use and allows to do all the view stuff in the java code " +
                 "(in contrast to using JSPs or templates or whatever in other frameworks)."
            }
        };
    }
}
