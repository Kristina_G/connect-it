﻿using ConnectIT.Data.Entities;
using Microsoft.AspNetCore.Identity;
using System;

namespace ConnectIT.Data.Seeds
{
    internal static class UserSeeds
    {
        private static readonly PasswordHasher<User> hasher = new PasswordHasher<User>();

        public static readonly User[] admins =
        {
            CreateUser(
                id: "988d99d9-ccb5-4f72-b6b4-c318cb1c6805",
                mail: "kristina.gizdova@connectit.com",
                fullName: "Kristina Gizdova",
                birthDay: new DateTime(2020, 10, 1),
                nationality: "Bulgarian",
                job: "SiteAdmin",
                profilePicture: PhotoSeeds.programmer),
            CreateUser(
                id: "4f3c09b2-f4c6-46c0-9b0b-216cb08f6184",
                mail: "petar.gavrilov@connectit.com",
                fullName: "Petar Gavrilov",
                birthDay: new DateTime(2020, 10, 1),
                nationality: "Bulgarian",
                job: "SiteAdmin",
                profilePicture: PhotoSeeds.programmer)
        };

        public static readonly User[] regular =
        {
            CreateUser(
                id: "24cb77bf-a074-46ba-b3e5-27e81e52d73a",
                mail: "kristina@google.com",
                fullName: "Kristina Gizdova",
                birthDay: new DateTime(1992, 12, 18),
                nationality: "Bulgarian",
                job: "WebDeveloper",
                profilePicture: PhotoSeeds.programmer),
            CreateUser(
                id: "fd1ac498-a477-40bf-8526-7c2208adbad5",
                mail: "petar@google.com",
                fullName: "Petar Gavrilov",
                birthDay: new DateTime(1994, 12, 5),
                nationality: "Bulgarian",
                job: "WebDeveloper",
                profilePicture: PhotoSeeds.programmer),
            CreateUser(
                id: "9b8effa0-4c8b-4bc8-b3d4-4af0153dbfa8",
                mail: "starshinata@google.com",
                fullName: "Strahil Ivanov",
                birthDay: new DateTime(1990, 10, 14),
                nationality: "Bulgarian",
                job: "WebDeveloper",
                profilePicture: PhotoSeeds.programmer),
            CreateUser(
                id: "2388891b-5a68-47bd-9b21-055c319ca23c",
                mail: "vankata98@connectit.com",
                fullName: "Ivan Todev",
                birthDay: new DateTime(1998, 6, 21),
                nationality: "Bulgarian",
                job: "WebDeveloper",
                profilePicture: PhotoSeeds.programmer)
        };

        // All friend request are for Petar
        public static readonly FriendRequest[] friendRequests =
        {
            // Petar sender
            new FriendRequest
            {
                SenderId = regular[1].Id,
                ReceiverId = regular[2].Id
            },
            new FriendRequest
            {
                SenderId = regular[1].Id,
                ReceiverId = regular[3].Id
            },
            // Petar receiver
            new FriendRequest
            {
                SenderId = regular[2].Id,
                ReceiverId = regular[1].Id
            },
            new FriendRequest
            {
                SenderId = regular[3].Id,
                ReceiverId = regular[1].Id,
     
            }
        };

        // All friend request are for Kristina
        public static readonly Friend[] friends =
        {
            // Kristina sender
            new Friend
            {
                SenderId = regular[0].Id,
                ReceiverId = regular[1].Id
            },
            new Friend
            {
                SenderId = regular[0].Id,
                ReceiverId = regular[2].Id
            },
            new Friend
            {
                SenderId = regular[0].Id,
                ReceiverId = regular[3].Id
            },
            // Kristina receiver
            new Friend
            {
                SenderId = regular[1].Id,
                ReceiverId = regular[0].Id
            },
            new Friend
            {
                SenderId = regular[2].Id,
                ReceiverId = regular[0].Id
            },
            new Friend
            {
                SenderId = regular[3].Id,
                ReceiverId = regular[0].Id
            }
        };

        private static User CreateUser(string id, string mail, string fullName, DateTime birthDay, string nationality, string job, byte[] profilePicture)
        {
            var normalizedMail = mail.ToUpper();
            var names = fullName.Split();

            var user = new User
            {
                Id = id,
                UserName = mail,
                NormalizedUserName = normalizedMail,
                Email = mail,
                NormalizedEmail = normalizedMail,
                FirstName = names[0],
                LastName = names[1],
                BirthDate = birthDay,
                Nationality = nationality,
                Job = job,
                ProfilePicture = profilePicture,
                CreatedOn = new DateTime(2020, 10, 1)
            };

            user.PasswordHash = hasher.HashPassword(user, "temp-password");

            return user;
        }


    }
}
