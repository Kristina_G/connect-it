﻿using ConnectIT.Data.Entities;
using System;

namespace ConnectIT.Data.Seeds
{
    internal static class ReactionSeeds
    {
        public static readonly Reaction[] reactions =
        {
            new Reaction
            {
                Id = 1,
                Reacton = Enums.ReactonStatus.Heart,
                PostId = 1,
                CreatedOn = new DateTime(2020, 11, 14),
                UserId = UserSeeds.regular[1].Id
            },
            new Reaction
            {
                Id = 2,
                Reacton = Enums.ReactonStatus.ThumbsUp,
                PostId = 1,
                CreatedOn = new DateTime(2020, 11, 15),
                UserId=UserSeeds.regular[3].Id
            },
            new Reaction
            {
                Id = 3,
                Reacton = Enums.ReactonStatus.Heart,PostId=2,
                CreatedOn = new DateTime(2020, 11, 16),
                UserId = UserSeeds.regular[0].Id
            },
            new Reaction
            {
                Id = 4,
                Reacton = Enums.ReactonStatus.ThumbsUp,
                PostId = 2,
                CreatedOn = new DateTime(2020, 11, 17),
                UserId = UserSeeds.regular[2].Id
            },
            new Reaction
            {
                Id = 5,
                Reacton = Enums.ReactonStatus.ThumbsUp,
                PostId = 3,
                CreatedOn = new DateTime(2020, 11, 18),
                UserId = UserSeeds.regular[1].Id
            }
        };
    }
}
