﻿using ConnectIT.Data.Entities;
using System;

namespace ConnectIT.Data.Seeds
{
    internal static class CommentReplySeeds
    {
        public static readonly Comment[] firstCommentReplies =
        {
            new Comment
            {
                UserId = UserSeeds.regular[2].Id,
                CreatedOn = DateTime.Now.AddDays(-1),
                Description = "Is Laravel free? ",
                Id = 2
            }
        };
    }
}
