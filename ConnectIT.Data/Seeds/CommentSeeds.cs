﻿using ConnectIT.Data.Entities;
using System;

namespace ConnectIT.Data.Seeds
{
    internal static class CommentSeeds
    {
        public static readonly Comment[] comments =
        {
            new Comment
            {
                UserId = UserSeeds.regular[1].Id,
                CreatedOn = new DateTime(2020, 11, 10),
                Description = "I have used it and I believe it is one of the best MVC web apps. ",
                Id = 1,
                PostId = PostSeeds.posts[0].Id
            },
            new Comment
            {
                UserId = UserSeeds.regular[0].Id,
                CreatedOn = new DateTime(2020, 11, 11),
                Description = "Really,but is it pleasant to work with Java?I don't think so.. ",
                Id = 2,
                PostId = PostSeeds.posts[2].Id
            },
            new Comment
            {
                UserId = UserSeeds.regular[3].Id,
                CreatedOn = new DateTime(2020, 11, 12),
                Description = "I have heard Laravel is good,but personally I prefer Java based frameworks :)",
                Id = 3,
                PostId = PostSeeds.posts[0].Id
            }
        };
    }
}
