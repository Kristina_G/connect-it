﻿using Microsoft.AspNetCore.Identity;

namespace ConnectIT.Data.Seeds
{
    internal static class RoleSeeds
    {
        public static readonly IdentityRole[] roles =
        {
            new IdentityRole
            {
                Id = "781a51b3-f134-4112-87f3-13b31f67ff27",
                Name = "admin",
                NormalizedName="ADMIN"
            }
        };

        public static readonly IdentityUserRole<string>[] adminsMapping =
        {
            new IdentityUserRole<string>()
            {
                RoleId = roles[0].Id,
                UserId = UserSeeds.admins[0].Id
            },

            new IdentityUserRole<string>()
            {
                RoleId = roles[0].Id,
                UserId = UserSeeds.admins[1].Id
            }
        };
    }
}
