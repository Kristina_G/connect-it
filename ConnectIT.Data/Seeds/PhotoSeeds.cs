﻿using ConnectIT.Data.Entities;
using System.IO;

namespace ConnectIT.Data.Seeds
{
    internal static class PhotoSeeds
    {
        public static string currentDir = Directory.GetCurrentDirectory();

        public static byte[] programmer = File.ReadAllBytes(currentDir + "/Pictures/Programmer.jpg");
        public static byte[] programmerAvatar = File.ReadAllBytes(currentDir + "/Pictures/ProgrammerAvatar.jpg");

        public static byte[] angular = File.ReadAllBytes(currentDir + "/Pictures/Angular.png");
        public static byte[] laravel = File.ReadAllBytes(currentDir + "/Pictures/Laravel.png");
        public static byte[] wickedJava = File.ReadAllBytes(currentDir + "/Pictures/JavaWicked.jpg");

        public static readonly Photo[] photos =
        {
              new Photo
              {
                  Id = 1,
                  Bytes = laravel,
                  PostId = 1
              },
              new Photo
              {
                  Id = 2,
                  Bytes = angular,
                  PostId = 2
              },
              new Photo
              {
                  Id = 3,
                  Bytes = wickedJava,
                  PostId = 3
              }
        };
    }
}