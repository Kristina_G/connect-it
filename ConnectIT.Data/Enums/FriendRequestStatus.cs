﻿namespace ConnectIT.Data.Enums
{
    public enum FriendRequestStatus
    {
        Pending = 0,
        Accepted = 1,
        Declined = 2
    }
}
