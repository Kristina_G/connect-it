﻿namespace ConnectIT.Data.Enums
{
    public enum ReactonStatus
    {
        ThumbsUp = 0,
        Heart = 1,
        Laughing = 2,
        Angry = 3
    }
}
