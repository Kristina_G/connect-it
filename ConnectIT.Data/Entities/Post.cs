﻿using ConnectIT.Data.Entities.Abstracts;
using System.Collections.Generic;

namespace ConnectIT.Data.Entities
{
    public class Post : Entity
    {
        public int Id { get; set; }

        public string AuthorId { get; set; }

        public User Author { get; set; }

        public string Content { get; set; }

        public string Location { get; set; }

        public bool PubliclyVisible { get; set; } = true;

        public ICollection<Photo> Photos { get; set; } = new List<Photo>();

        public ICollection<Friend> Tags { get; set; } = new List<Friend>();

        public ICollection<Comment> Comments { get; set; } = new List<Comment>();

        public ICollection<Reaction> Reactions { get; set; } = new List<Reaction>();
    }
}
