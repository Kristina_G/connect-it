﻿using ConnectIT.Data.Entities.Abstracts;
using System.Collections.Generic;

namespace ConnectIT.Data.Entities
{
    public class Comment : Entity
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public string UserId { get; set; }

        public User User { get; set; }

        public ICollection<Comment> Replies { get; set; }

        public int PostId { get; set; }

    }
}
