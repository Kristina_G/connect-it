﻿namespace ConnectIT.Data.Entities
{
    public class Photo
    {
        public int Id { get; set; }

        public byte[] Bytes { get; set; }

        public int PostId { get; set; }

        public Post Post { get; set; }
    }
}
