﻿using ConnectIT.Data.Enums;
using System.ComponentModel.DataAnnotations;

namespace ConnectIT.Data.Entities
{
    public class FriendRequest
    {
        public string SenderId { get; set; }
        public User Sender { get; set; }
        public string ReceiverId { get; set; }
        public User Receiver { get; set; }

    }
}
