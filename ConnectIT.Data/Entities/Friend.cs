﻿namespace ConnectIT.Data.Entities
{
    public class Friend
    {
        public string SenderId { get; set; }
        public User Sender { get; set; }

        public string ReceiverId { get; set; }
        public User Receiver { get; set; }
    }
}