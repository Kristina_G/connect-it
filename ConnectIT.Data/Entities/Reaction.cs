﻿using ConnectIT.Data.Entities.Abstracts;
using ConnectIT.Data.Enums;

namespace ConnectIT.Data.Entities
{
    public class Reaction : Entity
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public User User { get; set; }

        public int PostId { get; set; }

        public Post Post { get; set; }

        public ReactonStatus Reacton { get; set; }
    }
}
