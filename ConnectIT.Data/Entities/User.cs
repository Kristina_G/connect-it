﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ConnectIT.Data.Entities
{
    public class User : IdentityUser
    {
        [Required, StringLength(255)]
        public string FirstName { get; set; }

        [Required, StringLength(255)]
        public string LastName { get; set; }

        public DateTime BirthDate { get; set; }

        public string Nationality { get; set; }

        public string Job { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;

        public DateTime? ModifiedOn { get; set; }

        public DateTime? DeletedOn { get; set; }

        public bool IsDeleted { get; set; }

        public bool HideProfileDetails { get; set; } = false;

        public byte[] ProfilePicture { get; set; }

        public ICollection<FriendRequest> FriendRequests { get; set; } = new List<FriendRequest>();

        public ICollection<Friend> Friends { get; set; } = new List<Friend>();

        public ICollection<Comment> Comments { get; set; } = new List<Comment>();

        public ICollection<Post> MyPosts { get; set; } = new List<Post>();

        public ICollection<Reaction> Reactions { get; set; } = new List<Reaction>();
    }
}
