﻿using ConnectIT.Data.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ConnectIT.Data.Seeds;
using Microsoft.AspNetCore.Identity;
using System.Linq;

namespace ConnectIT.Data
{
    public class ConnectITDBContext : IdentityDbContext<User>
    {
        public ConnectITDBContext(DbContextOptions<ConnectITDBContext> options) : base(options) { }

        public DbSet<Post> Posts { get; set; }

        public DbSet<FriendRequest> FriendRequests { get; set; }

        public DbSet<Friend> Friend{ get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Set up relations which EF can`t figure out
            builder.Entity<Friend>()
                .HasKey(x => new { x.SenderId, x.ReceiverId });

            builder.Entity<Friend>()
                .HasOne(x => x.Sender)
                .WithMany(x => x.Friends)
                .HasForeignKey(x => x.SenderId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<FriendRequest>()
                .HasKey(x => new { x.SenderId, x.ReceiverId });

            builder.Entity<FriendRequest>()
                .HasOne(x => x.Sender)
                .WithMany(x => x.FriendRequests)
                .HasForeignKey(x => x.SenderId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<Post>()
                .HasOne(x => x.Author)
                .WithMany(x => x.MyPosts)
                .HasForeignKey(x => x.AuthorId);

            // Seed application data
            builder.Entity<IdentityRole>().HasData(RoleSeeds.roles);

            builder.Entity<User>().HasData(UserSeeds.admins);

            builder.Entity<User>().HasData(UserSeeds.regular);

            builder.Entity<IdentityUserRole<string>>().HasData(RoleSeeds.adminsMapping);

            builder.Entity<FriendRequest>().HasData(UserSeeds.friendRequests);

            builder.Entity<Friend>().HasData(UserSeeds.friends);

            builder.Entity<Post>().HasData(PostSeeds.posts);

            builder.Entity<Photo>().HasData(PhotoSeeds.photos);

            builder.Entity<Comment>().HasData(CommentSeeds.comments);

            builder.Entity<Reaction>().HasData(ReactionSeeds.reactions);
        }
    }
}
