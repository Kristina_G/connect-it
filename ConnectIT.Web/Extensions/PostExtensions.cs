﻿using ConnectIT.DataTransfer;
using System.Collections.Generic;
using System.Linq;
using ConnectIT.Web.Constants;

namespace ConnectIT.Web.Extensions
{
    public static class PostExtensions
    {
        public static string GetReactionStyles(this IEnumerable<ReactionDetail> reactions, string userId, int reactionCode) =>
            reactions.FirstOrDefault(x => x.ReactionCode == reactionCode && x.UserId == userId) != null ?
            Styles.ActiveBadgeStyles : Styles.StandardBadgeStyles;

        public static int CountWithCode(this IEnumerable<ReactionDetail> reactions, int reactionCode) =>
           reactions.Count(x => x.ReactionCode == reactionCode);
    }
}
