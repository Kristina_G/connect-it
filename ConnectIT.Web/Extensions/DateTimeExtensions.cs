﻿using System;

namespace ConnectIT.Web.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToDisplayFormat(this DateTime date) => date.ToString("ddd MMM dd yyyy");
    }
}
