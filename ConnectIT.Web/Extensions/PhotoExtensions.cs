﻿using ConnectIT.DataTransfer;
using System;

namespace ConnectIT.Web.Extensions
{
    public static class PhotoExtensions
    {
        public static string ToBase64(this PhotoDetail photo) => Convert.ToBase64String(photo.Bytes);
    }
}
