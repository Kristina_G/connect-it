﻿using ConnectIT.DataTransfer;
using ConnectIT.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;

namespace ConnectIT.Web.Controllers
{
    public class NewsFeedController : Controller
    {
        private readonly IPostService postService;

        public NewsFeedController(IPostService postService)
        {
            this.postService = postService;
        }

        private string UserId => User.FindFirstValue(ClaimTypes.NameIdentifier);

        // GET: newsfeed
        [HttpGet]
        public IActionResult Index()
        {
            IEnumerable<PostDetail> posts;
            if (User.IsInRole("admin"))
            {
                posts = postService.GetAll();
            }
            else if (User.Identity.IsAuthenticated)
            {
                posts = postService.GetNewsFeed(UserId);
            }
            else
            {
                posts = postService.GetPublic();
            }

            return View(posts);
        }

        // GET: newsfeed/partial/id
        [HttpGet("newsfeed/partial/{postId}")]
        public IActionResult GetOlderPosts(int postId)
        {
            IEnumerable<PostDetail> posts;
            if (User.IsInRole("admin"))
            {
                posts = postService.GetAll(postId);
            }
            else if (User.Identity.IsAuthenticated)
            {
                posts = postService.GetNewsFeed(UserId, postId);
            }
            else
            {
                posts = postService.GetPublic(postId);
            }

            return PartialView("_PostCollectionPartial", posts);
        }
    }
}
