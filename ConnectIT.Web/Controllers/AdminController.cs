﻿using ConnectIT.Services.Contracts;
using ConnectIT.Web.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace ConnectIT.Web.Controllers
{
    [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    public class AdminController : Controller
    {
        private readonly IPostService postService;
        private readonly ICommentService commentService;
        private readonly IUserService userService;

        public AdminController(IPostService postService, ICommentService commentService, IUserService userService)
        {
            this.postService = postService;
            this.commentService = commentService;
            this.userService = userService;
        }

        [HttpGet("admin/editpost/{id}")]
        public IActionResult EditPost(int id)
        {
            var dto = postService.Get(id);

            var model = new PostEditModel
            {
                Id = dto.Id,
                AuthorId = dto.Author.Id,
                CreatedOn = dto.CreatedOn,
                Content = dto.Content,
                Location = dto.Location,
                Photos = dto.Photos,
                Comments = dto.Comments.Select(x => new CommentInputModel
                {
                    Id = x.Id,
                    PostId = x.PostId,
                    AuthorName = x.AuthorName,
                    CreatedOn = x.CreatedOn,
                    Content = x.Text
                })
            };

            return View(model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult EditPost(PostEditModel model)
        {
            if (ModelState.IsValid)
            {
                postService.UpdatePost(model.Id, model.Location, model.Content);

                return RedirectToAction("EditPost", model.Id);
            }

            return View(model);
        }

        [HttpPut, ValidateAntiForgeryToken]
        public IActionResult UpdateComment(CommentInputModel model)
        {
            commentService.UpdateComment(model.PostId, model.Id, model.Content);

            return Ok("Updated");
        }

        [HttpPost, ValidateAntiForgeryToken] // HTML form.submit does not bind with DELETE method
        public IActionResult DeletePost(int id)
        {
            postService.DeletePost(id);

            return RedirectToAction("index", "newsfeed");
        }

        [HttpDelete, ValidateAntiForgeryToken]
        public IActionResult DeleteComment(CommentInputModel model)
        {
            commentService.DeleteComment(model.PostId, model.Id);

            return Ok("Deleted");
        }

        [HttpDelete]
        public IActionResult DeletePhoto(int postId, int photoId)
        {
            postService.DeletePostPhoto(postId, photoId);

            return Ok("Deleted");
        }
    }
}
