﻿using ConnectIT.DataTransfer;
using ConnectIT.Services.Contracts;
using ConnectIT.Web.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System.Security.Claims;

namespace ConnectIT.Web.Controllers
{
    [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
    public class WallController : Controller
    {
        private readonly IPostService postService;
        private readonly IFriendService friendService;

        public WallController(IPostService postService, IFriendService friendService)
        {
            this.postService = postService;
            this.friendService = friendService;
        }

        private string UserId => User.FindFirstValue(ClaimTypes.NameIdentifier);

        // GET: wall
        [HttpGet]
        public IActionResult Index()
        {
            return View(postService.GetWall(UserId));
        }

        // GET: wall/partial/id
        [HttpGet("wall/partial/{postId}")]
        public IActionResult GetOlderPosts(int postId)
        {
            return PartialView("_PostCollectionPartial", postService.GetWall(UserId, postId));
        }

        // GET: wall/create
        public IActionResult Create()
        {
            var model = new PostInputModel();

            model.Friends = friendService.GetFriends(UserId)
                .Select(x => new SelectListItem($"{x.FirstName} {x.LastName}", x.Id));

            return View(model);
        }

        // POST: wall/create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(PostInputModel model)
        {
            if (ModelState.IsValid)
            {
                model.AuthorId = UserId;

                postService.CreatePost((PostDetail)model);

                return RedirectToAction("Index");
            }

            return View(model);
        }
    }
}
