﻿using ConnectIT.Services.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ConnectIT.Web.Controllers
{
    public class FriendRequestController : Controller
    {
        private readonly IFriendRequestService friendRequestService;
        private readonly IUserService userService;
        public readonly IFriendService friendService;

        public FriendRequestController(IFriendRequestService friendRequestService, IUserService userService,IFriendService friendService)
        {
            this.friendRequestService = friendRequestService;
            this.userService = userService;
            this.friendService = friendService;
        }

        [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Index()
        {

            var userId = User.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value;

            var users = friendRequestService.GetFriendRequests(userId);

            return View(users);

        }


        public IActionResult AddFriend(string senderId, string receiverId)
        {

            senderId= User.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value;

            if (this.friendService.Exists(senderId, receiverId))
            {
                TempData["message"] = "This user is already in your friendlist";
            }

            else
            {
                if (this.friendRequestService.Exists(senderId, receiverId))
                {
                    TempData["message"] = "You have already sent friend request to this user";
                }

                else
                {
                    if (this.friendRequestService.Exists(receiverId, senderId))
                    {
                        TempData["message"] = "This user has already sent you friendrequest.Please check your friendrequest page";
                    }
                    else
                    {
                        TempData["message"] = "FriendRequest sent";
                        this.friendRequestService.Create(senderId, receiverId);
                    }
                }
            }
           
            return RedirectToAction("Details","Users", new { id= receiverId });
        }

        public IActionResult Accept(string senderId, string receiverId)
        {
            TempData["message"] = "Added new Friend";
            this.friendRequestService.Accept(senderId, receiverId);
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult Decline(string senderId, string receiverId)
        {
            TempData["message"] = "Friendship Declined";
            this.friendRequestService.Decline(senderId, receiverId);
            return RedirectToAction(nameof(Index));
        }

  

    }
}

