﻿using ConnectIT.DataTransfer;
using ConnectIT.Services.Contracts;
using ConnectIT.Web.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Linq;

namespace ConnectIT.Web.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUserService userService;

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        public IActionResult Index(UserListModel model, string searchBy, string searchString)
        {
            if (model == null)
            {
                model = new UserListModel();
            }
            model.Users = this.userService.GetUsers().Select(user => new UserSummaryModel(user));

            if (searchBy == "Name")
            {
                if (!string.IsNullOrEmpty(searchString))
                {
                    model.Users = model.Users.Where(s => s.FullName.ToLower().Contains(searchString.ToLower()));
                }
            }

            if (searchBy == "Job")
            {
                if (!string.IsNullOrEmpty(searchString))
                {
                    model.Users = model.Users.Where(t => t.Job != null && t.Job.ToLower().Contains(searchString.ToLower()));
                }
            }

            return View(model);
        }

        public IActionResult Details(string id)
        {
            var dto = this.userService.GetUserById(id);
            var model = new UserSummaryModel(dto);
            return View(model);
        }

        // GET: users/list
        [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
        [Authorize(Roles = "admin"), HttpGet]
        public IActionResult List()
        {
            var users = this.userService.GetUsersIncludingDeleted();

            return View(users);
        }

        [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
        [Authorize(Roles = "admin")]
        public IActionResult DeleteUser([FromForm] string userId)
        {
            this.userService.DeleteUser(userId);
            return RedirectToAction(nameof(List));
        }

        [Authorize(Roles = "admin")]
        public IActionResult RestoreUser(string userId)
        {
            this.userService.RestoreUser(userId);
            return RedirectToAction(nameof(List));
        }

        [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
        [Authorize(Roles = "admin")]
        public IActionResult Edit(string Id)
        {
            var dto = userService.GetOneUserbyId(Id);
            var model = new UserEditModel(dto);

            return View(model);
        }

        [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost]
        [ValidateAntiForgeryToken]

        [Authorize(Roles = "admin")]
        public IActionResult Edit(string Id, UserDetail model)
        {
            if (ModelState.IsValid)
            {

                if (Request.Form.Files.Count > 0)
                {
                    IFormFile file = Request.Form.Files.FirstOrDefault();
                    using (var dataStream = new MemoryStream())
                    {
                        file.CopyTo(dataStream);

                        model.ProfilePhoto = dataStream.ToArray();

                    }
                }

                userService.UpdateUser(model);
                TempData["message"] = "User Details Updated";
                return RedirectToAction("Edit");
            }

            return View(model);
        }
    }
}
