﻿using ConnectIT.Services.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ConnectIT.Web.Controllers
{
    public class FriendController : Controller
    {
        private readonly IFriendService friendService;
        private readonly IUserService userService;

        public FriendController(IFriendService friendService, IUserService userService)
        {
            this.friendService = friendService;
            this.userService = userService;
        }

        [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Index()
        {

            var userId = User.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value;

            var users = this.friendService.GetFriends(userId);

            return View(users);

        }

        [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult RemoveFriend(string senderId, string receiverId)
        {
           
            senderId = User.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value;
            this.friendService.RemoveFriend(receiverId, senderId);
            TempData["message"] = "Friend Removed";
            return RedirectToAction(nameof(Index));
        }




    }
}
