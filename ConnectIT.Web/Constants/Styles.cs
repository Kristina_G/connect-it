﻿namespace ConnectIT.Web.Constants
{
    public static class Styles
    {
        public const string ActiveBadgeStyles = "btn badge badge-pill btn-info";
        public const string StandardBadgeStyles = "btn badge badge-pill btn-outline-info";
    }
}
