﻿var isFetchingPosts = false;

const apiCaller = {
    'publishComment': (payload, callback) => {
        $.ajax({
            url: '../api/post/comment',
            data: payload,
            method: 'POST'
        }).done(callback);
    },
    'toggleReaction': (payload, callback) => {
        $.ajax({
            url: '../api/post/togglereaction',
            data: payload,
            method: 'POST'
        }).done(callback);
    },
    'fetchPreviousPosts': (latestPostId, callback) => {
        $.ajax({
            url: getCurrentPage() + '/partial/' + latestPostId,
            dataType: 'html',
            method: 'GET'
        }).done(callback);
    },
    'deleteComment': ($form, callback) => {
        var action = '../deletecomment';
        $form.attr('action', action);
        $form.attr('method', 'DELETE');

        $.ajax({
            url: action,
            data: $form.serialize(),
            method: 'DELETE'
        }).done(callback);
    },
    'updateComment': ($form) => {
        var action = '../updatecomment';
        $form.attr('action', action);
        $form.attr('method', 'PUT');

        $.ajax({
            url: action,
            data: $form.serialize(),
            method: 'PUT'
        });
    },
    'deletePhoto': (payload, callback) => {
        $.ajax({
            url: '../deletephoto',
            data: payload,
            method: 'DELETE'
        }).done(callback);
    }
};

const userInteraction = {
    'toggleReaction': ($reaction, postId) => {
        var payload = {
            'reactionCode': $reaction.data('code'),
            'postId': postId
        };

        apiCaller.toggleReaction(payload, responseData => userInterface.updateReaction(responseData, $reaction));
    },
    'createComment': (postId) => {
        commentModal.setPostId(postId);
        commentModal.show();
    },
    'publishComment': () => {
        var payload = {
            'postId': commentModal.getPostId(),
            'content': commentModal.getText()
        };

        apiCaller.publishComment(payload, responseData => userInterface.updateComments(responseData, payload.postId));

        commentModal.hide();
        commentModal.clear();
    },
    'scrollToEnd': () => {
        var oldestPostId = $('#post-container').children().last().data('postId');

        apiCaller.fetchPreviousPosts(oldestPostId, responseData => eventHandlers.morePostsHandler(responseData));
    },
    'updateComment': ($updateButton) => {
        var currentForm = $updateButton.closest('form');

        apiCaller.updateComment(currentForm);
    },
    'deleteComment': ($deleteButton) => {
        var currentForm = $deleteButton.closest('form');

        apiCaller.deleteComment(currentForm, () => currentForm.remove());
    },
    'deletePhoto': ($deleteButton) => {
        var currentCard = $deleteButton.closest('.card');
        var payload = currentCard.data();

        apiCaller.deletePhoto(payload, () => currentCard.remove());
    }
};

const userInterface = {
    'updateReaction': (count, $button) => {
        $button.toggleClass('btn-info btn-outline-info');
        $button.children().last().text(count);
    },
    'updateComments': (comments, postId) => {
        var commentsContainer = $(`#comments-${postId}`);

        commentsContainer.html(comments);
    },
    'updatePosts': (posts) => {
        $('#post-container').append(posts);
    }
};

const eventHandlers = {
    'scrollEndListener': () => {
        var scrolledToEnd = Math.round($(window).scrollTop() + $(window).height()) >= $(document).height();

        if (scrolledToEnd && isFetchingPosts == false) {
            processingRequest = true;

            userInteraction.scrollToEnd();
        }
    },
    'morePostsHandler': (posts) => {
        if (posts === '') {
            // remove the scroll listener because all posts were loaded
            $(document).off('scroll', eventHandlers.scrollEndListener);
        } else {
            userInterface.updatePosts(posts);
        }

        isFetchingPosts = false;
    }
};

const commentModal = (() => {
    const modal = $('#comment-dialog');
    const input = $('#comment-text');

    return {
        'getPostId': () => modal.data('postId'),
        'setPostId': value => modal.data('postId', value),
        'show': () => modal.modal('show'),
        'hide': () => modal.modal('hide'),
        'clear': () => input.val(''),
        'getText': () => input.val()
    };
})();

const getCurrentPage = () => {
    var url = window.location.href;
    var page = 'NewsFeed';

    if (url.endsWith('Wall')) {
        page = 'Wall';
    }

    return page;
};

$(document).ready(() => {
    $(document).on('scroll', eventHandlers.scrollEndListener);
});