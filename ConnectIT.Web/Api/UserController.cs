﻿using ConnectIT.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using ConnectIT.DataTransfer;
using Microsoft.AspNetCore.Authorization;
using ConnectIT.Web.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Linq;

namespace ConnectIT.Web.Api
{
   
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService service;
        private readonly IFriendService friendService;

        public UserController(IUserService service, IFriendService friendService)
        {
            this.service = service;
            this.friendService = friendService;
        }

        // Post: api/user/authenticate
        [HttpPost("authenticate")]
        [AllowAnonymous]
        public IActionResult Authenticate([FromBody] LoginCredentials model)
        {
            var token = this.service.GetAuthToken(model.Email, model.Password);

            if (token == null)
            {
                return BadRequest(new { message = "Email or password is incorrect" });
            }

            return Ok(token);
        }

        // GET: api/user
        [HttpGet]
        public IActionResult GetUsers()
        {
            return Ok(service.GetUsers());
        }

        // GET: api/user/email
        [HttpGet("{email}")]
        public IActionResult GetUser(string email)
        {
            UserDetail dto = service.GetUser(email);

            if (dto == null)
            {
                return NotFound();
            }

            return Ok(dto);
        }


        // GET: api/user/friends
        [HttpGet("friends")]
        [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]

        public IActionResult GetUserFrends()
        {
            var userId = User.FindFirstValue(ClaimTypes.Name);

            return Ok(friendService.GetFriends(userId));
        }

        // PUT: api/users
        [HttpPut]
        [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult PutUser([FromBody] UserInputModel model)
        {
            if (!service.Exists(model.Email))
            {
                return NotFound();
            }

            var userId = User.FindFirstValue(ClaimTypes.Name);

            service.UpdateUser(userId, model.Email, model.Password);

            return Ok("User updated");
        }

        // POST: api/users
        [HttpPost]
        [AllowAnonymous]
        public IActionResult PostUser([FromBody] UserInputModel model)
        {
            service.CreateUser(model.Email, model.Password,model.FirstName,model.LastName);

            return Ok("User created");
        }
    }
}
