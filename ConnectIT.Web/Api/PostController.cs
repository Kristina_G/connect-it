﻿using ConnectIT.DataTransfer;
using ConnectIT.Services.Contracts;
using ConnectIT.Web.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace ConnectIT.Web.Api
{
    [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : Controller
    {
        private readonly IPostService _postService;
        private readonly ICommentService _commentService;
        private readonly IReactionService _reactionService;

        public PostController(IPostService postService, ICommentService commentService, IReactionService reactionService)
        {
            _postService = postService;
            _commentService = commentService;
            _reactionService = reactionService;
        }

        private string UserId => User.FindFirstValue(ClaimTypes.NameIdentifier);

        // POST: api/post
        [HttpPost]
        public IActionResult CreatePost([FromBody] PostInputModel model)
        {
            model.AuthorId = UserId;

            _postService.CreatePost((PostDetail)model);

            return Ok("Post created");
        }

        // GET: api/post/wall
        [HttpGet("wall")]
        public IActionResult GetWall()
        {
            return Ok(_postService.GetWall(UserId));
        }

        // GET: api/post/feed
        [HttpGet("feed")]
        public IActionResult GetNewsFeed()
        {
            return Ok(_postService.GetNewsFeed(UserId));
        }

        // POST: api/post/togglereaction
        [HttpPost("togglereaction")]
        public IActionResult ToggleReaction([FromForm] ReactionInputModel model)
        {
            if (_reactionService.HasUserReacted(UserId, model.PostId, model.ReactionCode))
            {
                _reactionService.Delete(UserId, model.PostId, model.ReactionCode);
            }
            else
            {
                _reactionService.Create(UserId, model.PostId, model.ReactionCode);
            }

            return Ok(_reactionService.Count(model.PostId, model.ReactionCode));
        }

        // POST: api/post/comment
        [HttpPost("comment")]
        public IActionResult CreateComment([FromForm] CommentInputModel model)
        {
            _commentService.CreateComment(UserId, model.PostId, model.Content);
            var comments = _commentService.GetComments(model.PostId);

            return PartialView("_CommentCollectionPartial", comments);
        }
    }
}
