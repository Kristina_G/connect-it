﻿using ConnectIT.DataTransfer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text.Json.Serialization;

namespace ConnectIT.Web.Models
{
    public class PostInputModel
    {
        [JsonIgnore]
        public string AuthorId { get; set; }

        [Required, StringLength(1000)]
        public string Content { get; set; }

        [StringLength(100)]
        public string Location { get; set; }

        public bool PubliclyVisible { get; set; }

        public IFormFile[] Photos { get; set; }

        public IEnumerable<string> TaggedFriends { get; set; } = new List<string>();

        public IEnumerable<SelectListItem> Friends { get; set; }

        public static explicit operator PostDetail(PostInputModel model)
        {
            var dto = new PostDetail()
            {
                Author = new UserSummary { Id = model.AuthorId },
                Content = model.Content,
                Location = model.Location,
                PubliclyVisible = model.PubliclyVisible,
                Photos = ConvertToPhotoDTO(model.Photos),
                TaggedFriends = model.TaggedFriends.Select(x => new UserSummary { Id = x })
            };

            return dto;
        }

        private static IEnumerable<PhotoDetail> ConvertToPhotoDTO(IFormFile[] photos)
        {
            var result = new List<PhotoDetail>();
            if (photos == null)
            {
                return result;
            }

            foreach (var photo in photos)
            {
                using (var memoryStream = new MemoryStream())
                {
                    photo.CopyTo(memoryStream);

                    result.Add(new PhotoDetail
                    {
                        Bytes = memoryStream.ToArray()
                    });
                };
            }

            return result;
        }
    }
}
