﻿using ConnectIT.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConnectIT.Web.Models
{
    public class UserSummaryModel
    {
        public UserSummaryModel(UserDetail dto)
        {
            Id = dto.Id;
            Email = dto.Email;
            FirstName = dto.FirstName;
            ProfilePhoto = dto.ProfilePhoto;
            LastName = dto.LastName;
            Job = dto.Job;
            FullName = dto.FullName;
            HideProfileDetails = dto.HideProfileDetails;
            FriendIds = dto.FriendIds;
            BirthDate = dto.BirthDate;
            Nationality = dto.Nationality;
        }

        public string Id { get; set; }
        public string FirstName { get; set; }
        public byte[] ProfilePhoto { get; set; }
        public string LastName { get; set; }
        public string Job { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public bool HideProfileDetails { get; set; }
        public DateTime BirthDate { get; set; }
        public string Nationality { get; set; }

        public IEnumerable<string> FriendIds { get; set; }

        public bool AreDetailsVisibleToUser(string viewerId)
        {
            if (HideProfileDetails == false)
            {
                return true;
            }

            return FriendIds.Contains(viewerId);
        }
    }
}
