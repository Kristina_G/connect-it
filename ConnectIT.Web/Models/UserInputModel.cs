﻿using System.ComponentModel.DataAnnotations;

namespace ConnectIT.Web.Models
{
    public class UserInputModel
    {
        [Required]
        public string Email { get; set; }

        [Required, StringLength(12, MinimumLength = 6)]
        public string Password { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
    }
}
