﻿using ConnectIT.DataTransfer;
using System;
using System.ComponentModel.DataAnnotations;

namespace ConnectIT.Web.Models
{
    public class UserEditModel
    {
        public UserEditModel(UserDetail dto)
        {
            Id = dto.Id;
            FirstName = dto.FirstName;
            LastName = dto.LastName;
            BirthDate = dto.BirthDate;
            Nationality = dto.Nationality;
            ProfilePic = dto.ProfilePhoto;
            Job = dto.Job;
            Email = dto.Email;
            HideProfileDetails = dto.HideProfileDetails;
        }

        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }

        public DateTime BirthDate { get; set; }

        public string Job { get; set; }

        public string Nationality { get; set; }

        public byte[] ProfilePic { get; set; }

        public bool HideProfileDetails { get; set; }

        public static explicit operator UserDetail(UserEditModel model) => new UserDetail()
        {
            Id = model.Id,
            FirstName = model.FirstName,
            LastName = model.LastName,
            BirthDate = model.BirthDate,
            Nationality = model.Nationality,
            ProfilePhoto = model.ProfilePic,
            Job = model.Job,
            Email = model.Email,
            HideProfileDetails = model.HideProfileDetails
        };
    }
}

