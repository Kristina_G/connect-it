﻿using System.ComponentModel.DataAnnotations;

namespace ConnectIT.Web.Models
{
    public class ReactionInputModel
    {
        [Required]
        public int PostId { get; set; }

        [Range(0, 3)]
        public int ReactionCode { get; set; }
    }
}
