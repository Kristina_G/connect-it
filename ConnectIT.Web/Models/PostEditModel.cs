﻿using ConnectIT.DataTransfer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ConnectIT.Web.Models
{
    public class PostEditModel
    {
        public int Id { get; set; }

        public string AuthorId { get; set; }

        public DateTime CreatedOn { get; set; }

        [Required, StringLength(1000)]
        public string Content { get; set; }

        [StringLength(100)]
        public string Location { get; set; }

        public IEnumerable<PhotoDetail> Photos { get; set; }

        public IEnumerable<CommentInputModel> Comments { get; set; }
    }
}
