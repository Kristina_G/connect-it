﻿using ConnectIT.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConnectIT.Web.Models
{
    public class UserListModel
    {
        public IEnumerable<UserSummaryModel> Users { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string FullName { get; set; }
        public string Job { get; set; }

        public string Id { get; set; }

    }
}
