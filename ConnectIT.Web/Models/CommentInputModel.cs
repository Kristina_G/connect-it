﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ConnectIT.Web.Models
{
    public class CommentInputModel
    {
        public int Id { get; set; }

        [Required]
        public int PostId { get; set; }

        [Required]
        public string Content { get; set; }

        public string AuthorName { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
