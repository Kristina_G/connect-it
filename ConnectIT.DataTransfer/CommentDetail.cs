﻿using System;

namespace ConnectIT.DataTransfer
{
    public class CommentDetail
    {
        public int Id { get; set; }

        public int PostId { get; set; }

        public string AuthorName { get; set; }

        public string Text { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
