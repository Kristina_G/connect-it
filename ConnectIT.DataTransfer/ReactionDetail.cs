﻿namespace ConnectIT.DataTransfer
{
    public class ReactionDetail
    {
        public int PostId { get; set; }

        public string UserId { get; set; }

        public int ReactionCode { get; set; }
    }
}
