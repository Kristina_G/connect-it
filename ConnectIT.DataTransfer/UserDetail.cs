﻿using System;
using System.Collections.Generic;

namespace ConnectIT.DataTransfer
{
    public class UserDetail : UserSummary
    {
        public string Email { get; set; }

        public DateTime BirthDate { get; set; }

        public string Nationality { get; set; }

        public string Job { get; set; }

        public bool IsDeleted { get; set; }

        public IEnumerable<string> FriendIds { get; set; }
    }
}
