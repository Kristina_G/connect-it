﻿using System;
using System.Collections.Generic;

namespace ConnectIT.DataTransfer
{
    public class PostDetail
    {
        public int Id { get; set; }

        public DateTime CreatedOn { get; set; }

        public UserSummary Author { get; set; }

        public string Content { get; set; }

        public string Location { get; set; }

        public bool PubliclyVisible { get; set; }

        public IEnumerable<PhotoDetail> Photos { get; set; }

        public IEnumerable<UserSummary> TaggedFriends { get; set; } = new List<UserSummary>();

        public IEnumerable<ReactionDetail> Reactions { get; set; } = new List<ReactionDetail>();

        public IEnumerable<CommentDetail> Comments { get; set; } = new List<CommentDetail>();
    }
}
