﻿using ConnectIT.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConnectIT.DataTransfer
{
    public class FriendRequestDetails
    {
        public string SenderEmail { get; set; }
        public string SenderId { get; set; }

        public string ReceiverId { get; set; }
        public string SenderFullName { get; set; }

        public byte[] SenderPhoto { get; set; }
    }
}
