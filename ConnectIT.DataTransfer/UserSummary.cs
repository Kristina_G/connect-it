﻿namespace ConnectIT.DataTransfer
{
    public class UserSummary
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public byte[] ProfilePhoto { get; set; }

        public bool HideProfileDetails { get; set; }

        public string FullName => $"{FirstName} {LastName}";
    }
}
