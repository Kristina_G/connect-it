﻿namespace ConnectIT.DataTransfer
{
    public class PhotoDetail
    {
        public int Id { get; set; }

        public byte[] Bytes { get; set; }
    }
}