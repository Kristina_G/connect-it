﻿using ConnectIT.DataTransfer;
using System.Collections.Generic;

namespace ConnectIT.Services.Contracts
{
    public interface IFriendService
    {
        public IEnumerable<UserSummary> GetFriends(string userId);

        public void MakeFriends(string senderId, string receiverId);

        public bool Exists(string senderId, string receiverId);

        public void RemoveFriend(string senderId, string receiverId);

    }
}
