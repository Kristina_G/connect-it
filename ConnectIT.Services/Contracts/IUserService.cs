﻿using ConnectIT.DataTransfer;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace ConnectIT.Services.Contracts
{
    public interface IUserService
    {
        public string GetAuthToken(string username, string password);

        public IEnumerable<UserDetail> GetUsers();

        public IEnumerable<UserDetail> GetUsersIncludingDeleted();

        public UserDetail GetUser(string email);

        public UserDetail GetUserById(string email);

        public void UpdateUser(string userId, string email, string password);

        public void CreateUser(string email, string password,string FirstName,string LastName);

        public void DeleteUser(string userId);

        public void RestoreUser(string userId);

        public bool Exists(string email);

        bool UserExists(string senderId);
  
        public UserDetail GetOneUserbyId(string userId);

        public void UpdateUser(UserDetail dto);

    }
}
