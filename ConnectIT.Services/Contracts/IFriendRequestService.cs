﻿using ConnectIT.DataTransfer;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConnectIT.Services.Contracts
{
   public interface IFriendRequestService
    {
        public void Create(string senderId, string receiverId);

        public void Accept(string senderId, string receiverId);

        public void Decline(string senderId, string receiverId);

        public IEnumerable<FriendRequestDetails> GetFriendRequests(string receiverId);

        public bool Exists(string senderId, string receiverId);
    }
}
