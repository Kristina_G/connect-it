﻿using ConnectIT.DataTransfer;
using System.Collections.Generic;

namespace ConnectIT.Services.Contracts
{
    public interface ICommentService
    {
        public void CreateComment(string userId, int postId, string text);

        public IEnumerable<CommentDetail> GetComments(int postId);

        void UpdateComment(int postId, int id, string content);

        void DeleteComment(int postId, int id);
    }
}
