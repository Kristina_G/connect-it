﻿using System;

namespace ConnectIT.Services.Providers
{
    public interface IDateTimeProvider
    {
        DateTime GetDateTime();
    }
}