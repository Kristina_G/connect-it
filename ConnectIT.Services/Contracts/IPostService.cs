﻿using ConnectIT.DataTransfer;
using System.Collections.Generic;

namespace ConnectIT.Services.Contracts
{
    public interface IPostService
    {
        public void CreatePost(PostDetail postDetail);

        public PostDetail Get(int postId);

        public void UpdatePost(int postId, string location, string content);

        public void DeletePost(int postId);

        public void DeletePostPhoto(int postId, int photoId);

        /// <summary>
        /// Returns latest user news feed
        /// </summary>
        /// <returns>Most recent posts created by user friends</returns>
        public IEnumerable<PostDetail> GetNewsFeed(string userId);

        /// <summary>
        /// Returns older user news feed
        /// </summary>
        /// <returns>Posts created created by user friends before provided postId</returns>
        public IEnumerable<PostDetail> GetNewsFeed(string userId, int postId);

        /// <summary>
        /// Returns latest user own posts
        /// </summary>
        /// <returns>Most recent posts created by user</returns>
        public IEnumerable<PostDetail> GetWall(string userId);

        /// <summary>
        /// Returns older user own posts
        /// </summary>
        /// <returns>Posts created by user before provided postId</returns>
        public IEnumerable<PostDetail> GetWall(string userId, int postId);

        /// <summary>
        /// Gets most recent public posts
        /// </summary>
        /// <returns>Most recent publicly visible posts</returns>
        public IEnumerable<PostDetail> GetPublic();

        /// <summary>
        /// Gets older public posts
        /// </summary>
        /// <returns>Publicly visible posts that were created before postId</returns>
        public IEnumerable<PostDetail> GetPublic(int postId);

        public IEnumerable<PostDetail> GetAll();

        public IEnumerable<PostDetail> GetAll(int postId);
    }
}
