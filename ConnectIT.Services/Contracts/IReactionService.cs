﻿namespace ConnectIT.Services.Contracts
{
    public interface IReactionService
    {
        public void Create(string userId, int postId, int reactionCode);

        public void Delete(string userId, int postId, int reactionCode);

        public bool HasUserReacted(string userId, int postId, int reactionCode);

        public int Count(int postId, int reactionCode);
    }
}
