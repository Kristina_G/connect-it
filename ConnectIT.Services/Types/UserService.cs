﻿using ConnectIT.Data;
using ConnectIT.Data.Entities;
using ConnectIT.DataTransfer;
using ConnectIT.DataTransfer.Helpers;
using ConnectIT.Services.Contracts;
using ConnectIT.Services.Providers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ConnectIT.Services.Types
{
    public class UserService : IUserService, IUserConfirmation<User>
    {
        private readonly ConnectITDBContext context;
        private readonly ApplicationSettings applicationSettings;
        private readonly IPasswordHasher<User> passwordHasher;
        private readonly IDateTimeProvider dateTimeProvider;

        public UserService(ConnectITDBContext context, IDateTimeProvider dateTimeProvider, IOptions<ApplicationSettings> applicationSettings, IPasswordHasher<User> passwordHasher)
        {
            this.context = context;
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
            this.applicationSettings = applicationSettings.Value;
            this.passwordHasher = passwordHasher;
        }

        public string GetAuthToken(string email, string password)
        {
            var user = context.Users.SingleOrDefault(x => x.Email == email);

            if (user == null)
                return null;

            var passwordCheck = new PasswordHasher<User>().VerifyHashedPassword(user, user.PasswordHash, password);

            if (passwordCheck.ToString() != "Success")
            {
                return null;
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(applicationSettings.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var userToken = context.UserTokens.SingleOrDefault(x => x.UserId == user.Id);

            if (userToken == null)
            {
                userToken = context.UserTokens.Add(new IdentityUserToken<string>
                {
                    LoginProvider = "JWT-Custom",
                    UserId = user.Id,
                    Name = "API-token"
                }).Entity;
            }

            userToken.Value = tokenHandler.WriteToken(token);

            context.SaveChanges();

            return userToken.Value;
        }

        public IEnumerable<UserDetail> GetUsers()
        {
            return context.Users
                .Include(user => user.Friends)
                .Where(user => user.IsDeleted == false)
                .OrderBy(user => user.FirstName)
                .ThenBy(user => user.LastName)
                .Select(ConvertToDTO);
        }

        public IEnumerable<UserDetail> GetUsersIncludingDeleted()
        {
            return context.Users
                .Include(user => user.Friends)
                .OrderBy(user => user.FirstName)
                .ThenBy(user => user.LastName)
                .Select(ConvertToDTO);
        }

        public UserDetail GetUser(string email)
        {
            var entity = context.Users
                .Include(user => user.Friends)
                .Where(entity => entity.IsDeleted == false)
                .FirstOrDefault(entity => entity.Email == email);

            if (entity == null)
            {
                return null;
            }

            return ConvertToDTO(entity);
        }

        public UserDetail GetOneUserbyId(string userId)
        {
            var entity = context.Users
                .Include(user => user.Friends)
                .FirstOrDefault(entity => entity.Id == userId);

            if (entity == null)
            {
                return null;
            }

            return ConvertToDTO(entity);
        }

        public UserDetail GetUserById(string Id)
        {
            var entity = context.Users
                .Include(user => user.Friends)
                .Where(entity => entity.IsDeleted == false)
                .FirstOrDefault(entity => entity.Id == Id);

            if (entity == null)
            {
                return null;
            }

            return ConvertToDTO(entity);
        }

        public void UpdateUser(string userId, string email, string password)
        {
            var entity = context.Users
                .Where(u => u.IsDeleted == false)
                .FirstOrDefault(user => user.Id == userId);

            if (!string.IsNullOrWhiteSpace(password))
            {
                entity.PasswordHash = passwordHasher.HashPassword(entity, password);
            }

            if (!string.IsNullOrWhiteSpace(email))
            {
                var normalizedEmail = email.ToUpper();
                entity.UserName = email;
                entity.NormalizedUserName = normalizedEmail;
                entity.Email = email;
                entity.NormalizedEmail = normalizedEmail;
                entity.ModifiedOn = dateTimeProvider.GetDateTime();
              
            }

            if (context.ChangeTracker.HasChanges())
            {
                context.SaveChanges();
            }
        }

        public void CreateUser(string email, string password,string FirstName,string LastName)
        {
            var normalizedEmail = email.ToUpper();

            var entity = new User
            {
                UserName = email,
                NormalizedUserName = normalizedEmail,
                Email = email,
                NormalizedEmail = normalizedEmail,
                CreatedOn = dateTimeProvider.GetDateTime(),
                IsDeleted = false,
                FirstName=FirstName,
                LastName=LastName
            };

            entity.PasswordHash = passwordHasher.HashPassword(entity, password);

            context.Users.Add(entity);
            context.SaveChanges();
        }

        public void DeleteUser(string userId)
        {
            var entity = context.Users
                 .Where(user => user.IsDeleted == false)
                 .FirstOrDefault(user => user.Id == userId);

            if (entity == null)
            {
                return;
            }

            entity.IsDeleted = true;
            entity.DeletedOn = dateTimeProvider.GetDateTime();
            context.SaveChanges();

        }

        public void RestoreUser(string userId)
        {
            var entity = context.Users
                 .Where(user => user.IsDeleted == true)
                 .FirstOrDefault(user => user.Id == userId);

            if (entity == null)
            {
                return;
            }

            entity.IsDeleted = false;
            entity.ModifiedOn = dateTimeProvider.GetDateTime();
            context.SaveChanges();


        }

        public bool Exists(string email)
        {
            return context.Users
                .Where(entity => entity.IsDeleted == false)
                .FirstOrDefault(entity => entity.Email == email) != null;
        }

        public bool UserExists(string userId) => this.context.Users.Any(u => u.Id == userId && u.IsDeleted == false);

        public Task<bool> IsConfirmedAsync(UserManager<User> userManager, User user) =>
         Task.FromResult(user.IsDeleted == false);

        public void UpdateUser(UserDetail dto)
        {

            var entity = context.Users.FirstOrDefault(x => x.Id == dto.Id);

            if (entity == null)
            {
                throw new ArgumentException("User was not found");
            }

            entity.Id = dto.Id;
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.Job = dto.Job;
            entity.BirthDate = dto.BirthDate;
            entity.Nationality = dto.Nationality;
            entity.Email = dto.Email;
            if (dto.ProfilePhoto != null)
            {
                entity.ProfilePicture = dto.ProfilePhoto;
            }

            entity.ModifiedOn = dateTimeProvider.GetDateTime();
            context.SaveChanges();

        }

        private static UserDetail ConvertToDTO(User entity) => new UserDetail
        {
            Id = entity.Id,
            Email = entity.Email,
            FirstName = entity.FirstName,
            LastName = entity.LastName,
            BirthDate = entity.BirthDate,
            Nationality = entity.Nationality,
            IsDeleted=entity.IsDeleted,
            Job = entity.Job,
            ProfilePhoto = entity.ProfilePicture,
            HideProfileDetails = entity.HideProfileDetails,
            FriendIds = entity.Friends.Select(x => x.ReceiverId)
        };
    }
}