﻿using ConnectIT.Data;
using ConnectIT.Data.Entities;
using ConnectIT.DataTransfer;
using ConnectIT.Services.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace ConnectIT.Services.Types
{
    public class FriendRequestService : IFriendRequestService
    {
        private readonly ConnectITDBContext context;
        private readonly IUserService userService;
        private readonly IFriendService friendService;

        public FriendRequestService(ConnectITDBContext context, IUserService userService, IFriendService friendService)
        {
            this.context = context;
            this.userService = userService;
            this.friendService = friendService;
        }
        public void Accept(string senderId, string receiverId)
        {
            var friendRequest = context.FriendRequests.FirstOrDefault(fr => fr.ReceiverId == receiverId && fr.SenderId == senderId);
            this.friendService.MakeFriends(senderId, receiverId);
            this.context.SaveChanges();
        }

        public void Create(string senderId, string receiverId)
        {
            if (!this.Exists(senderId, receiverId) && this.userService.UserExists(senderId) && this.userService.UserExists(receiverId))
            {
                var friendRequest = new FriendRequest
                {
                    SenderId = senderId,
                    ReceiverId = receiverId,

                };

                this.context.FriendRequests.Add(friendRequest);
                this.context.SaveChanges();
            }
        }

        public void Decline(string senderId, string receiverId)
        {

            var friendRequest = context.FriendRequests.FirstOrDefault(fr => fr.ReceiverId == receiverId && fr.SenderId == senderId);
            this.context.Remove(friendRequest);
            this.context.SaveChanges();

        }

        public bool Exists(string senderId, string receiverId) =>
              this.context.FriendRequests.Any(fr => fr.SenderId == senderId && fr.ReceiverId == receiverId);

        public IEnumerable<FriendRequestDetails> GetFriendRequests(string receiverId)
        {
            var entity = context.FriendRequests
                .Where(entity => entity.ReceiverId == receiverId)
                .Select(entity => new FriendRequestDetails
                {
                    SenderFullName = $"{entity.Sender.FirstName} {entity.Sender.LastName}",
                    SenderEmail = entity.Sender.Email,
                    SenderPhoto = entity.Sender.ProfilePicture,
                    SenderId = entity.SenderId,
                    ReceiverId = entity.ReceiverId

                });

            return entity;
        }
    }
}
