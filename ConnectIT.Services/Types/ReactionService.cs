﻿using ConnectIT.Data;
using ConnectIT.Data.Entities;
using ConnectIT.Data.Enums;
using ConnectIT.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ConnectIT.Services.Types
{
    public class ReactionService : IReactionService
    {
        private readonly ConnectITDBContext context;

        public ReactionService(ConnectITDBContext context)
        {
            this.context = context;
        }

        public void Create(string userId, int postId, int reactionCode)
        {
            var post = GetPostWithReactions(postId);

            var status = (ReactonStatus)reactionCode;

            post.Reactions.Add(new Reaction
            {
                Reacton = status,
                UserId = userId
            });

            context.SaveChanges();
        }

        public void Delete(string userId, int postId, int reactionCode)
        {
            var post = GetPostWithReactions(postId);

            var status = (ReactonStatus)reactionCode;

            var reaction = post.Reactions
                .First(x => x.UserId == userId && x.Reacton == status);

            post.Reactions.Remove(reaction);

            context.SaveChanges();
        }

        public int Count(int postId, int reactionCode)
        {
            var status = (ReactonStatus)reactionCode;

            return GetPostWithReactions(postId).Reactions.Count(x => x.Reacton == status);
        }

        public bool HasUserReacted(string userId, int postId, int reactionCode)
        {
            var status = (ReactonStatus)reactionCode;

            return GetPostWithReactions(postId).Reactions
                .FirstOrDefault(x => x.UserId == userId && x.Reacton == status) != null;
        }

        private Post GetPostWithReactions(int postId) =>
            context.Posts.Include(post => post.Reactions).First(post => post.Id == postId);
    }
}
