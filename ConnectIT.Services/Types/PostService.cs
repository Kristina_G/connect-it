﻿using ConnectIT.Data;
using ConnectIT.Data.Entities;
using ConnectIT.DataTransfer;
using ConnectIT.Services.Contracts;
using ConnectIT.Services.Providers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConnectIT.Services.Types
{
    public class PostService : IPostService
    {
        private readonly ConnectITDBContext context;
        private readonly IDateTimeProvider dateTimeProvider;

        public PostService(ConnectITDBContext context, IDateTimeProvider dateTimeProvider)
        {
            this.context = context;
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
        }

        public void CreatePost(PostDetail input)
        {
            var authorEntity = context.Users
                .Include(user => user.Friends)
                .First(user => user.Id == input.Author.Id);

            var postEntity = new Post
            {
                Author = authorEntity,
                Content = input.Content,
                Location = input.Location,
                PubliclyVisible = input.PubliclyVisible,
                CreatedOn = dateTimeProvider.GetDateTime(),
                IsDeleted = false
            };

            foreach (var photo in input.Photos)
            {
                postEntity.Photos.Add(new Photo { Bytes = photo.Bytes });
            }

            foreach (var friend in input.TaggedFriends)
            {
                postEntity.Tags.Add(authorEntity.Friends.First(x => x.ReceiverId == friend.Id));
            }

            authorEntity.MyPosts.Add(postEntity);

            context.SaveChanges();
        }

        public IEnumerable<PostDetail> GetNewsFeed(string userId)
        {
            var userFriends = context.Users
                .Include(x => x.Friends)
                .ThenInclude(x => x.Receiver)
                .First(x => x.Id == userId).Friends.Select(x => x.ReceiverId);

            return FilterPosts(post => userFriends.Contains(post.AuthorId) || post.PubliclyVisible);
        }

        public IEnumerable<PostDetail> GetNewsFeed(string userId, int postId)
        {
            var userFriends = context.Users
               .Include(x => x.Friends)
               .ThenInclude(x => x.Receiver)
               .First(x => x.Id == userId).Friends.Select(x => x.ReceiverId);

            return FilterPosts(post => post.Id < postId && (userFriends.Contains(post.AuthorId) || post.PubliclyVisible));
        }

        public IEnumerable<PostDetail> GetWall(string userId) =>
            FilterPosts(post => post.AuthorId == userId);

        public IEnumerable<PostDetail> GetWall(string userId, int postId) =>
            FilterPosts(post => post.AuthorId == userId && post.Id < postId);

        public IEnumerable<PostDetail> GetPublic() =>
            FilterPosts(post => post.PubliclyVisible);

        public IEnumerable<PostDetail> GetPublic(int postId) =>
            FilterPosts(post => post.PubliclyVisible && post.Id < postId);

        public IEnumerable<PostDetail> GetAll() =>
            FilterPosts(post => true);

        public IEnumerable<PostDetail> GetAll(int postId) =>
            FilterPosts(post => post.Id < postId);

        public PostDetail Get(int postId) =>
            FilterPosts(post => post.Id == postId).Single();

        private static PostDetail ConvertToPostDetail(Post post)
        {
            return new PostDetail
            {
                Id = post.Id,
                Author = new UserSummary
                {
                    Id = post.Author.Id,
                    FirstName = post.Author.FirstName,
                    LastName = post.Author.LastName
                },
                Content = post.Content,
                Location = post.Location,
                CreatedOn = post.CreatedOn,
                Photos = post.Photos.Select(x => new PhotoDetail
                {
                    Id = x.Id,
                    Bytes = x.Bytes
                }),
                TaggedFriends = post.Tags.Select(x => new UserSummary
                {
                    Id = x.ReceiverId,
                    FirstName = x.Receiver.FirstName,
                    LastName = x.Receiver.LastName
                }),
                Reactions = post.Reactions.Select(x => new ReactionDetail
                {
                    PostId = x.PostId,
                    UserId = x.UserId,
                    ReactionCode = (int)x.Reacton
                }),
                Comments = post.Comments
                .Where(x => !x.IsDeleted)
                .OrderByDescending(x => x.CreatedOn)
                .Select(x => new CommentDetail
                {
                    Id = x.Id,
                    PostId = x.PostId,
                    AuthorName = x.User.FirstName,
                    Text = x.Description,
                    CreatedOn = x.CreatedOn
                })
            };
        }

        private IEnumerable<PostDetail> FilterPosts(Func<Post, bool> postPredicate)
        {
            return context.Posts
                .Include(post => post.Photos)
                .Include(post => post.Tags)
                    .ThenInclude(tag => tag.Receiver)
                .Include(post => post.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.User)
                .Where(x => !x.IsDeleted)
                .Where(postPredicate)
                .OrderByDescending(post => post.Id)
                .ThenBy(post => post.Reactions.Count())
                .Take(5)
                .Select(ConvertToPostDetail);
        }

        public void UpdatePost(int postId, string location, string content)
        {
            var post = context.Posts.First(x => x.Id == postId);
            post.Location = location;
            post.Content = content;
            post.ModifiedOn = dateTimeProvider.GetDateTime();

            context.SaveChanges();
        }

        public void DeletePost(int postId)
        {
            var post = context.Posts.First(x => x.Id == postId);
            post.IsDeleted = true;
            post.DeletedOn = dateTimeProvider.GetDateTime();

            context.SaveChanges();
        }

        public void DeletePostPhoto(int postId, int photoId)
        {
            var post = context.Posts
                .Include(post => post.Photos)
                .First(post => post.Id == postId);

            var photo = post.Photos.First(x => x.Id == photoId);

            context.Remove(photo);
            post.ModifiedOn = dateTimeProvider.GetDateTime();

            context.SaveChanges();
        }
    }
}
