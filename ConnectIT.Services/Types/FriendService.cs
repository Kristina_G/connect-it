﻿using ConnectIT.Data;
using ConnectIT.Data.Entities;
using ConnectIT.DataTransfer;
using ConnectIT.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ConnectIT.Services.Types
{
    public class FriendService : IFriendService
    {
        private readonly ConnectITDBContext context;

        public FriendService(ConnectITDBContext context)
        {
            this.context = context;
        }

        public bool Exists(string senderId, string receiverId) =>
              this.context.Friend.Any(fr => fr.SenderId == senderId && fr.ReceiverId == receiverId);

        public IEnumerable<UserSummary> GetFriends(string userId)
        {
            var user = context.Users
                 .Include(user => user.Friends)
                 .ThenInclude(x => x.Receiver)
                 .First(x => x.Id == userId);

            return user.Friends.Select(x => new UserSummary
            {
                Id = x.Receiver.Id,
                FirstName = x.Receiver.FirstName,
                LastName = x.Receiver.LastName,
                ProfilePhoto=x.Receiver.ProfilePicture

            });
        }

        public void MakeFriends(string senderId, string receiverId)
        {
            var friendrequest = this.context.FriendRequests.FirstOrDefault(x => x.SenderId == senderId && x.ReceiverId == receiverId);

            var userFriend = new Friend
            {
                ReceiverId = receiverId,
                SenderId = senderId
            };

            var otheruserFriend = new Friend
            {
                SenderId = receiverId,
                ReceiverId = senderId
            };

            this.context.Friend.Add(userFriend);
            this.context.Friend.Add(otheruserFriend);
            this.context.FriendRequests.Remove(friendrequest);
            this.context.SaveChanges();
        }


        public void RemoveFriend(string senderId, string receiverId)
        {
            var friendshipFirst = this.context.Friend.FirstOrDefault(x => x.SenderId == senderId && x.ReceiverId == receiverId);
            var friendshipSecond = this.context.Friend.FirstOrDefault(x => x.ReceiverId == senderId && x.SenderId == receiverId);
            this.context.Friend.Remove(friendshipFirst);
            this.context.Friend.Remove(friendshipSecond);
            this.context.SaveChanges();
        }

        
            


    }


}

