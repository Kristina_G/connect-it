﻿using ConnectIT.Data;
using ConnectIT.Data.Entities;
using ConnectIT.DataTransfer;
using ConnectIT.Services.Contracts;
using ConnectIT.Services.Providers;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ConnectIT.Services.Types
{
    public class CommentService : ICommentService
    {
        private readonly ConnectITDBContext context;
        private readonly IDateTimeProvider timeProvider;

        public CommentService(ConnectITDBContext context, IDateTimeProvider timeProvider)
        {
            this.context = context;
            this.timeProvider = timeProvider;
        }

        public void CreateComment(string authorId, int postId, string text)
        {
            var post = context.Posts.First(post => post.Id == postId);
            var author = context.Users.First(user => user.Id == authorId);

            var comment = new Comment
            {
                PostId = postId,
                User = author,
                Description = text
            };

            post.Comments.Add(comment);

            context.SaveChanges();
        }

        public void UpdateComment(int postId, int commentId, string content)
        {
            var post = context.Posts
                 .Include(post => post.Comments)
                 .First(post => post.Id == postId);

            var comment = post.Comments
                .First(comment => comment.Id == commentId);

            comment.Description = content;
            comment.ModifiedOn = timeProvider.GetDateTime();

            context.SaveChanges();
        }

        public void DeleteComment(int postId, int commentId)
        {
            var post = context.Posts
                 .Include(post => post.Comments)
                 .First(post => post.Id == postId);

            var comment = post.Comments
                .First(comment => comment.Id == commentId);

            comment.IsDeleted = true;
            comment.DeletedOn = timeProvider.GetDateTime();

            context.SaveChanges();
        }

        public IEnumerable<CommentDetail> GetComments(int postId)
        {
            var post = context.Posts
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.User)
                .First(post => post.Id == postId);

            return post.Comments
                .Where(x => !x.IsDeleted)
                .OrderByDescending(x => x.CreatedOn)
                .Select(x => new CommentDetail
                {
                    Id = x.Id,
                    PostId = x.PostId,
                    AuthorName = x.User.FirstName,
                    Text = x.Description,
                    CreatedOn = x.CreatedOn
                });
        }

    }
}
